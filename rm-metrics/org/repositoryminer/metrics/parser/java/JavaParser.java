package org.repositoryminer.metrics.parser.java;

import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.repositoryminer.metrics.ast.AST;
import org.repositoryminer.metrics.parser.Language;
import org.repositoryminer.metrics.parser.Parser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Java AST generator
 * 
 * This class has the job to create an abstract AST upon Java source code.
 * 
 * The extensions accepted for this generator are: java
 */
public class JavaParser extends Parser {

	private static final Logger LOG = LoggerFactory.getLogger(JavaParser.class);

	private static final String[] EXTENSIONS = { "java" };

	public JavaParser() {
		super.id = Language.JAVA;
		super.extensions = EXTENSIONS;
	}

	@Override
	public AST generate(String filename, String source, String[] srcFolders) {
		AST ast = null;

		@SuppressWarnings("deprecation")
		ASTParser parser = ASTParser.newParser(org.eclipse.jdt.core.dom.AST.JLS8);
		parser.setResolveBindings(true);
		parser.setKind(ASTParser.K_COMPILATION_UNIT);
		parser.setBindingsRecovery(true);
		parser.setCompilerOptions(JavaCore.getOptions());
		parser.setUnitName(filename);
		parser.setSource(source.toCharArray());

		parser.setEnvironment(null, srcFolders, null, true);

		try {
			CompilationUnit cu = (CompilationUnit) parser.createAST(null);

			FileVisitor visitor = new FileVisitor();
			cu.accept(visitor);

			ast = new AST();

			ast.setFileName(filename);
			ast.setSource(source);
			ast.setImports(visitor.getImports());
			ast.setPackageDeclaration(visitor.getPackageName());
			ast.setTypes(visitor.getTypes());
			ast.setLanguage(Language.JAVA);
			LOG.info("AST for '" + filename + "' generated successfully");
		} catch (Exception e) {
			LOG.error("Unable to generate AST for '" + filename + "'");
		}

		return ast;
	}

}
