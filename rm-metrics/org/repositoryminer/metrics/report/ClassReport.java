package org.repositoryminer.metrics.report;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bson.Document;

public class ClassReport {

	private String name;
	private String type;
	private String source;
	private boolean isInterface;
	private Map<String, FieldReport> fieldsReports = new HashMap<>();
	private Map<String, MethodReport> methodsReports = new HashMap<>();
	private MetricsReport metricsReport = new MetricsReport();

	public ClassReport(String name) {
		this.name = name;
	}

	public ClassReport(String name, String type) {
		this.name = name;
		this.type = type;
	}

	public ClassReport(String name, String type, boolean isInterface) {
		this.name = name;
		this.type = type;
		this.isInterface = isInterface;
	}

	public Collection<FieldReport> getFields() {
		return fieldsReports.values();
	}

	public Collection<MethodReport> getMethods() {
		return methodsReports.values();
	}

	public MethodReport getMethodBySignature(String signature) {
		MethodReport mr = methodsReports.get(signature);
		if (mr == null) {
			mr = new MethodReport(signature);
			methodsReports.put(signature, mr);
		}
		return mr;
	}

	public Document toDocument() {
		Document doc = new Document("name", name).append("type", type).append("is_interface", isInterface)
				.append("source", source).append("fields", FieldReport.toDocumentList(fieldsReports.values()))
				.append("metrics", metricsReport.toMetricsDocument())
				.append("codesmells", metricsReport.getCodeSmellsAsString())
				.append("methods", MethodReport.toDocumentList(methodsReports.values()));
		return doc;
	}

	public static Object toDocumentList(Collection<ClassReport> values) {
		List<Document> docs = new ArrayList<>();
		for (ClassReport cr : values) {
			docs.add(cr.toDocument());
		}
		return docs;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public boolean isInterface() {
		return isInterface;
	}

	public void setInterface(boolean isInterface) {
		this.isInterface = isInterface;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public Map<String, FieldReport> getFieldsReports() {
		return fieldsReports;
	}

	public void setFieldsReports(Map<String, FieldReport> fieldsReports) {
		this.fieldsReports = fieldsReports;
	}

	public Map<String, MethodReport> getMethodsReports() {
		return methodsReports;
	}

	public void setMethodsReports(Map<String, MethodReport> methodsReports) {
		this.methodsReports = methodsReports;
	}

	public MetricsReport getMetricsReport() {
		return metricsReport;
	}

	public void setMetricsReport(MetricsReport metricsReport) {
		this.metricsReport = metricsReport;
	}

}