package org.repositoryminer.metrics.report;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.bson.Document;

public class ImportReport {

	private String name;
	private boolean isStatic;

	public Document toDocument() {
		Document doc = new Document("name", name).append("static", isStatic);

		return doc;
	}

	public static Object toDocumentList(Collection<ImportReport> values) {
		List<Document> docs = new ArrayList<>();
		for (ImportReport ir : values) {
			docs.add(ir.toDocument());
		}

		return docs;
	}
	
	public ImportReport() {
		this("", false);
	}

	public ImportReport(String name) {
		this(name, false);
	}

	public ImportReport(String name, boolean isStatic) {
		this.name = name;
		this.isStatic = isStatic;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isStatic() {
		return isStatic;
	}

	public void setStatic(boolean isStatic) {
		this.isStatic = isStatic;
	}

}