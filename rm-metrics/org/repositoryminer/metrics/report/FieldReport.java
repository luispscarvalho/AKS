package org.repositoryminer.metrics.report;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.bson.Document;

public class FieldReport {

	private String name;
	private String type;
	private List<String> modifiers = new ArrayList<>();

	public Document toDocument() {
		Document doc = new Document("name", name).append("type", type).append("modifiers", modifiers);
		return doc;
	}

	public static Object toDocumentList(Collection<FieldReport> values) {
		List<Document> docs = new ArrayList<>();
		for (FieldReport fr : values) {
			docs.add(fr.toDocument());
		}
		return docs;
	}

	public FieldReport(String name, String type) {
		this.name = name;
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<String> getModifiers() {
		return modifiers;
	}

	public void setModifiers(List<String> modifiers) {
		this.modifiers = modifiers;
	}

}
