package org.repositoryminer.metrics.report;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bson.Document;
import org.repositoryminer.util.StringUtils;

public class FileReport {

	private String name;
	private String source;
	private Map<String, ClassReport> classesReports = new HashMap<>();
	private List<ImportReport> importReports = new ArrayList<>(); 
	private MetricsReport metricsReport = new MetricsReport();

	public ClassReport getClass(String name) {
		ClassReport cr = classesReports.get(name);
		if (cr == null) {
			cr = new ClassReport(name);
			classesReports.put(name, cr);
		}
		return cr;
	}

	public Collection<ClassReport> getClasses() {
		return classesReports.values();
	}
	
	public Document toDocument() {
		Document doc = new Document();
		doc.append("filename", name).
			append("filehash", StringUtils.encodeToCRC32(name)).
			append("source", source).
			append("imports", ImportReport.toDocumentList(importReports)).
			append("metrics", metricsReport.toMetricsDocument()).
			append("classes", ClassReport.toDocumentList(classesReports.values()));
		return doc;
	}

	public FileReport(String name, String source) {
		this.name = name;
		this.source = source;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Map<String, ClassReport> getClassesReports() {
		return classesReports;
	}

	public void setClassesReports(Map<String, ClassReport> classesReports) {
		this.classesReports = classesReports;
	}
	
	public List<ImportReport> getImportReports() {
		return importReports;
	}
	
	public void setImportReports(List<ImportReport> importReports) {
		this.importReports = importReports;
	}
	
	public MetricsReport getMetricsReport() {
		return metricsReport;
	}

	public void setMetricsReport(MetricsReport metricsReport) {
		this.metricsReport = metricsReport;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

}