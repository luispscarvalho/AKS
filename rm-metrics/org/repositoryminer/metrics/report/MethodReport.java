package org.repositoryminer.metrics.report;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bson.Document;
import org.repositoryminer.metrics.ast.NodeType;

public class MethodReport {

	private String name;
	private String source;
	private int beginsAt, endsAt;
	private List<String> modifiers = new ArrayList<>();
	private MetricsReport metricsReport = new MetricsReport();
	private Map<NodeType, Set<String>> statements = new HashMap<>();

	public MethodReport(String signature) {
		this.name = signature;
	}

	public Document toDocument() {
		Document doc = new Document("name", name).append("source", source).append("begins_at", beginsAt)
				.append("ends_at", endsAt).append("modifiers", modifiers)
				.append("metrics", metricsReport.toMetricsDocument())
				.append("codesmells", metricsReport.getCodeSmellsAsString());

		List<Document> docs = new ArrayList<>();
		for (Map.Entry<NodeType, Set<String>> entry : statements.entrySet()) {
			docs.add(new Document().append("nodetype", entry.getKey().toString()).append("expression",
					entry.getValue()));
		}
		doc.append("statements", docs);

		return doc;
	}

	public static Object toDocumentList(Collection<MethodReport> values) {
		List<Document> docs = new ArrayList<>();
		for (MethodReport mr : values) {
			docs.add(mr.toDocument());
		}
		return docs;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public int getBeginsAt() {
		return beginsAt;
	}

	public void setBeginsAt(int beginsAt) {
		this.beginsAt = beginsAt;
	}

	public int getEndsAt() {
		return endsAt;
	}

	public void setEndsAt(int endsAt) {
		this.endsAt = endsAt;
	}

	public List<String> getModifiers() {
		return modifiers;
	}

	public void setModifiers(List<String> modifiers) {
		this.modifiers = modifiers;
	}

	public Map<NodeType, Set<String>> getStatements() {
		return statements;
	}

	public void setStatements(Map<NodeType, Set<String>> statements) {
		this.statements = statements;
	}

	public MetricsReport getMetricsReport() {
		return metricsReport;
	}

	public void setMetricsReport(MetricsReport metricsReport) {
		this.metricsReport = metricsReport;
	}

}