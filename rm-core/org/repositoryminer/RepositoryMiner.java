package org.repositoryminer;

import java.io.IOException;

import org.repositoryminer.domain.Repository;
import org.repositoryminer.domain.SCMType;
import org.repositoryminer.persistence.RepositoryDAO;

/**
 * The front-end class to perform the repository data extraction.
 */
public class RepositoryMiner {

	private String key;
	private String path;
	private String name;
	private String description;
	private SCMType scm;

	public Repository mine() throws IOException {
		Repository repository = null;
		
		RepositoryDAO repoDocHandler = new RepositoryDAO();
		if (!repoDocHandler.wasMined(key)) {
			repository = RepositoryExtractor.run(this);
		} 
		
		return repository;
	}
	
	public RepositoryMiner() {
		this(null);
	}

	public RepositoryMiner(String key) {
		this.key = key;
	}

	public RepositoryMiner(String key, String path, String name,
			String description, SCMType scm) {
		this(key);
		this.path = path;
		this.name = name;
		this.description = description;
		this.scm = scm;
	}

	/*** Getters and Setters ***/

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public SCMType getSCM() {
		return scm;
	}

	public void setSCM(SCMType scm) {
		this.scm = scm;
	}

}
