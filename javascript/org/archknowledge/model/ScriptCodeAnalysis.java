package org.archknowledge.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.archknowledge.mining.ScriptMiner;
import org.bson.Document;

/**
 * Store information about javascript source code analysis.
 * <p>
 * It works together with {@link ScriptMiner}. {@link ScriptMiner} is the class
 * responsible for running analysis on .js files in which concerns are found. So
 * far, we have empowered it with only basic analysis strategies, as the one
 * that calculates the lines-of-code (LOC) of the files. 
 * <p>
 * TODO ideally, this class must be integrated with a proper javascript analyzer
 * in order to improve the mining and processing of .js files. For instance, the
 * new analyzer could enable the measurement of other metrics.
 * 
 * @author Luis Paulo
 *
 */
public class ScriptCodeAnalysis {
	private String path = "";
	private String source = "";

	private long LOC = 0;

	private Set<String> imports = new HashSet<>();

	public static List<Document> toDocuments(List<ScriptCodeAnalysis> infos) {
		List<Document> documents = new ArrayList<>();

		for (ScriptCodeAnalysis info : infos) {
			documents.add(info.toDocument());
		}

		return documents;
	}

	public Document toDocument() {
		Document doc = new Document();

		return doc.append("path", getPath()).append("imports", imports).append("metrics",
				new Document().append("LOC", getLOC()));
	}

	public ScriptCodeAnalysis(String path, String source, long LOC) {
		this.setPath(path);
		this.setSource(source);

		this.setLOC(LOC);
	}

	public String getPath() {
		return path;
	}

	public ScriptCodeAnalysis setPath(String path) {
		this.path = path;

		return this;
	}

	public String getSource() {
		return source;
	}

	public ScriptCodeAnalysis setSource(String source) {
		this.source = source;

		return this;
	}

	public long getLOC() {
		return LOC;
	}

	public ScriptCodeAnalysis setLOC(long lOC) {
		LOC = lOC;

		return this;
	}

	public Set<String> getImports() {
		return imports;
	}

	public ScriptCodeAnalysis setImports(Set<String> imports) {
		this.imports = imports;

		return this;
	}

	public ScriptCodeAnalysis clone() {
		return new ScriptCodeAnalysis(path, source, LOC);
	}
}
