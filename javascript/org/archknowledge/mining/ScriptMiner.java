package org.archknowledge.mining;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.archknowledge.model.ScriptCodeAnalysis;
import org.archknowledge.model.ThirdPartyComponent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;

/**
 * A basic javascript miner
 * <p>
 * As we have not found an adequate javascript source code analyzer yet, this
 * class is intended to encapsulate some basic .js files mining and processing
 * routines. It is capable of: (i) finding textual references to components in
 * the source code; (ii) calculating the loc of the files in which concerns are
 * found.
 * <p>
 * TODO ideally, this class must be replaced by a proper javascript analyzer in
 * order to improve the mining and processing of .js files. For instance, the
 * new analyzer could enable the measurement of other metrics.
 * 
 * @author Luis Paulo
 */
public class ScriptMiner {

	private static final Logger LOG = LoggerFactory.getLogger(ScriptMiner.class);

	private String path;
	private Map<String, ThirdPartyComponent> components;

	public ScriptMiner configure(String path) {
		this.path = path;

		return this;
	}

	public ScriptMiner configure(Map<String, ThirdPartyComponent> components) {
		this.components = components;

		return this;
	}

	/**
	 * Given the path of a javascript project, this method analyzes all .js files.
	 * Its main purpose is finding textual references to components in the files.
	 * <p>
	 * Once a reference is found, it creates a link/association between an artifact
	 * (a .js file), a concern, and an instance of {@link ScriptCodeAnalysis}.
	 * {@link ScriptCodeAnalysis} is used to store useful information about the
	 * association. For instance, the value of the lines-of-code metric.
	 * 
	 * @return a table linking each source code artifact, concerns, and an instance
	 *         of {@link ScriptCodeAnalysis}
	 * @throws Exception
	 */
	public Table<String, String, ScriptCodeAnalysis> execute() throws Exception {
		// ----concern file analysis
		Table<String, String, ScriptCodeAnalysis> analysisTable = HashBasedTable.create();

		Collection<File> scripts = findAllScripts();
		for (File script : scripts) {
			LOG.info("Mining '" + script.getAbsolutePath() + "'");

			ScriptCodeAnalysis codeAnalysis = analyze(script);
			codeAnalysis.setPath(script.getAbsolutePath());

			List<ThirdPartyComponent> referencedComponents = analyze(codeAnalysis.getSource());
			for (ThirdPartyComponent component : referencedComponents) {
				LOG.info("Referenced Component: '" + component.getName() + "' [" + component.getConcern() + "]");

				ScriptCodeAnalysis clone = codeAnalysis.clone();

				if (analysisTable.contains(component.getConcern(), codeAnalysis.getPath())) {
					clone = analysisTable.get(component.getConcern(), codeAnalysis.getPath());
				} else {
					analysisTable.put(component.getConcern(), codeAnalysis.getPath(), clone);
				}

				clone.getImports().add(component.getImport());
			}
		}

		return analysisTable;
	}

	/**
	 * This method determine which third-party components are imported/referenced by
	 * a javascript artifact's source code.
	 * <p>
	 * TODO we have very poorly defined a strategy to find references to a
	 * component. Preferably, we must find an adequate tool/framework/library to
	 * perform the static analysis of javascript projects' source code. Doing this
	 * may provide a more precise way to locate the references.
	 * 
	 * @param source
	 * @return list of third party components found in the source code
	 */
	private List<ThirdPartyComponent> analyze(String source) {
		List<ThirdPartyComponent> referenced = new ArrayList<>();

		for (ThirdPartyComponent component : components.values()) {
//			if (source.contains("require('" + component.getImport() + "')")
//					|| (source.contains("require(\"" + component.getImport() + "\")"))
//					|| (source.contains("from'" + component.getImport() + "'"))
//					|| (source.contains("from\"" + component.getImport() + "\""))) {
			if (source.contains("'" + component.getImport() + "'")
					|| (source.contains("\"" + component.getImport() + "\""))) {
				referenced.add(component);
			}
		}

		return referenced;
	}

	/**
	 * This method is responsible for: (i) retrieving the source code from a
	 * javascript artifact; (ii) calculating metrics (LOC) from the source code.
	 * 
	 * @param script a javascript artifact (.js file)
	 * @return an instance of {@link ScriptCodeAnalysis} that stores the source code
	 *         and the metrics
	 * @throws IOException
	 */
	private ScriptCodeAnalysis analyze(File script) throws IOException {
		String source = "", line = "";
		long LOC = Long.valueOf(0);

		BufferedReader br = new BufferedReader(new FileReader(script));
		while ((line = br.readLine()) != null) {
			source += line;
			LOC++;
		}
		br.close();

		return new ScriptCodeAnalysis("", source, LOC);
	}

	private Collection<File> findAllScripts() {
		return FileUtils.listFiles(new File(path), new String[] { "js", "JS" }, true);
	}

	public String getName() {
		return "Javascript Miner";
	}

}
