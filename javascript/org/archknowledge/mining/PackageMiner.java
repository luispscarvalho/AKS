package org.archknowledge.mining;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.Arrays;
import java.util.HashSet;
import java.util.stream.Collectors;

import org.archknowledge.model.ThirdPartyComponent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class mines metadata about javascript third-party components from
 * NPMRepository (https://www.npmjs.com/)
 * <p>
 * It parses the response of NPM after navigating to each component's NPM page.
 * This is done by concatenating the component's ID to the NPM's package URL:
 * https://www.npmjs.com/package/. For instance, the following URL shows
 * metadata information about the "node" framework:
 * <p>
 * https://www.npmjs.com/package/node
 * <p>
 * Specifically, this miner scans the resulting html for the information stored
 * within the "keywords" markup. The keywords are then injected in an instance
 * of {@link ThirdPartyComponent}. Doing this has the intention to use the
 * keywords as potential indicators of components' main purpose, so that an
 * adequate category and concern can be manually filled.
 * 
 * @author Luis Paulo
 *
 */
public class PackageMiner {
	private static final Logger LOG = LoggerFactory.getLogger(PackageMiner.class);

	private static final String NPM_URL = "https://www.npmjs.com/package/";
	private static final int NPM_ACCESS_INTERVAL = 10;

	private static final String[] TAGS_MARKUPS = new String[] {
			"<meta data-react-helmet=\"true\" name=\"keywords\" content=\"", "\"/>" };

	private ThirdPartyComponent component;

	public PackageMiner configure(ThirdPartyComponent component) {
		this.component = component;

		return this;
	}

	public PackageMiner mine() throws IOException {
		try {
			String html = getRawHTML();

			if (html != null) {
				fillTags(html);
			}

			Thread.sleep(NPM_ACCESS_INTERVAL * 1000);
		} catch (Exception e) {
			LOG.error("Error accessing MVNRepository: " + e.getMessage());
		}

		return this;
	}

	private String getRawHTML() throws IOException {
		String address = NPM_URL + component.getImport();
		LOG.info("Contacting NPM repository to mine metadata for: " + component.getName());

		URL url = new URL(address);
		URLConnection conn = url.openConnection();

		InputStream is = conn.getInputStream();
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		String html = br.lines().collect(Collectors.joining());

		return html;
	}

	private PackageMiner fillTags(String html) {
		component.setTags(new HashSet<>());

		int index = html.indexOf(TAGS_MARKUPS[0]);
		if (index >= 0) {
			html = html.substring(index + TAGS_MARKUPS[0].length());
			html = html.substring(0, html.indexOf(TAGS_MARKUPS[1]));
			html = html.trim().toLowerCase();

			if ((html != null) && !html.equals("") && !html.equals("none")) {
				String[] tags = html.split(",");
				component.getTags().addAll(Arrays.asList(tags));
			}
		}

		return this;
	}

}
