package org.archknowledge.export;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.archknowledge.persistence.AksDAO;
import org.bson.Document;

/**
 * This class exports information about javascript projects' concerns
 * <p>
 * For each artifact (.js files of javascript projects) it exports the list of
 * concerns found in it and the lines-of-code (loc) metric.
 * 
 * @author Luis Paulo
 */
public class ScriptConcernExporter extends Exporter {

	@Override
	public String configureHeader() {
		return "project,dimension,concern,imports,file,loc";
	}

	@SuppressWarnings("unchecked")
	@Override
	public Exporter export() throws Exception {
		prepare();

		AksDAO dao = AksDAO.newConcernDAO();
		List<Document> concerns = dao.findMany(null, null);

		for (Document concern : concerns) {
			List<Document> analysis = (List<Document>) concern.get("code_analysis");

			for (Document a : analysis) {
				Set<String> imports = new HashSet<>();
				imports.addAll((ArrayList<String>) a.get("imports"));

				String line = getProject() + "," + concern.getString("dimension") + "," + concern.getString("name")
						+ "," + allStringsInOneString(imports) + "," + a.getString("filename") + ","
						+ getMetricValue("LOC", (List<Document>) a.get("metrics"));

				getLines().add(line);
			}
		}

		return super.exportLines();
	}

}
