package org.archknowledge;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.archknowledge.config.Configurations;
import org.archknowledge.export.ScriptConcernExporter;
import org.archknowledge.mining.PackageMiner;
import org.archknowledge.mining.ScriptMiner;
import org.archknowledge.model.ScriptCodeAnalysis;
import org.archknowledge.model.ThirdPartyComponent;
import org.archknowledge.persistence.AksDAO;
import org.archknowledge.persistence.DAO;
import org.bson.Document;
import org.codehaus.plexus.util.FileUtils;
import org.repositoryminer.domain.Developer;
import org.repositoryminer.domain.Repository;
import org.repositoryminer.domain.SCMType;
import org.repositoryminer.persistence.RepositoryDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Table;

/**
 * Executor for mining JS projects
 * <p>
 * {@link JavaScriptExecutor} enables the following uses:
 * <p>
 * <ul>
 * <li>Mining - to extract concerns from components injected in javascript
 * source code files</li>
 * <li>Export - exports the information about concerns as CSV-base dataset
 * files</li>
 * </ul>
 * 
 * @author luisp
 */
public class JavaScriptExecutor {

	private static final Logger LOG = LoggerFactory.getLogger(JavaExecutor.class);

	private static Document config;
	private static List<ThirdPartyComponent> uncategorizedComponents;

	/**
	 * Main method of the executor. It activates the mining/export of projects or
	 * print the usage if it fails to parse a valid command line.
	 * 
	 * @param args mine/exporting arguments
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		if (args.length < 3) {
			printUsage();
		} else {
			String action = args[0];

			if (action.equals("mine")) {
				mine(args);
			} else if (action.equals("export")) {
				export(args);
			} else {
				printUsage();
			}
		}
	}

	/**
	 * This method a set of javascript projects
	 * <p>
	 * It depends on loading mining configuration files from the command line (see
	 * {@link #printUsage()}). Additionally, it seeks json package files in the
	 * projects' path. Package files contain information about third-party
	 * components injected in the projects. It also executes the code analysis based
	 * on the strategies embedded in {@link ScriptMiner}.
	 * <p>
	 * As it is necessary to perform the manual categorization of components, this
	 * method is also responsible for saving a json file to store information about
	 * uncategorized components.
	 * 
	 * @param args mine/exporting arguments
	 * @throws Exception
	 */
	private static void mine(String[] args) throws Exception {
		String configFile = args[1];
		config = loadConfiguration(configFile);

		uncategorizedComponents = new ArrayList<>();

		String[] paths = args[2].split(";");
		String[] dimensions = args[3].split(";");

		for (int i = 0; i < paths.length; i++) {
			String path = paths[i];
			LOG.info("Project path: " + path);

			String dimension = dimensions[i];
			LOG.info("Dimension: " + dimension);

			String packageFile = findJsonPackageFile(path);
			if (packageFile == null) {
				LOG.info("No 'package.json' found!");
			} else {
				Document jsonPackage = loadJsonPackage(packageFile);

				Repository repo = initRepository(jsonPackage);
				repo.setPath(path);

				LOG.info("Mining concerns from '" + repo.getName() + "'");

				save(dimension, repo, mine(path, jsonPackage));
			}
		}
		if (!uncategorizedComponents.isEmpty()) {
			saveUncategorizedComponents(uncategorizedComponents, configFile);
		}
	}

	/**
	 * This method executes an instance of {@link ScriptConcernExporter}
	 * <p>
	 * {@link ScriptConcernExporter} must be parameterized by the injection of a
	 * list of projects' keys. The keys are used to locate each project's database
	 * from which the information about concerns are retrieved.
	 * 
	 * @param args mine/exporting arguments
	 * @throws Exception
	 */
	private static void export(String[] args) throws Exception {
		String outputPath = args[1];
		String[] keys = args[2].split(";");

		ScriptConcernExporter exporter = new ScriptConcernExporter();

		List<String> allLines = new ArrayList<>();
		for (String key : keys) {
			String project = key.substring(key.indexOf('_') + 1);

			allLines.addAll(exporter.configure(project, outputPath, key, key).export().getLines());
		}

		exporter.setFileName("all").export(allLines);

	}

	/**
	 * This method performs the actual mining and analysis of javascript source code
	 * artifacts
	 * <p>
	 * It tries to find metadata about components in the local configuration file
	 * ("js.component.X.json"). In case no metadata is found, it activates the
	 * execution of {@link PackageMiner} to mine the metadata from NPM web site.
	 * <p>
	 * The second step runs and instance of {@link ScriptMiner} to extract metrics
	 * and other information about the projects' concerns.
	 * 
	 * @param path        path to a javascript project
	 * @param jsonPackage a json package file found in the project
	 * @return a table linking each source code artifact, concerns, and an instance
	 *         of {@link ScriptCodeAnalysis}
	 * @throws Exception
	 */
	private static Table<String, String, ScriptCodeAnalysis> mine(String path, Document jsonPackage) throws Exception {
		Map<String, String> localConcerns = loadConcerns(config);

		Map<String, ThirdPartyComponent> components = new HashMap<>();
		for (ThirdPartyComponent component : fromJSDependenciesToComponents(jsonPackage)) {
			if (!localConcerns.containsKey(component.getName())) {
				fillTags(component);
				uncategorizedComponents.add(component);
			} else {
				component.setConcern(localConcerns.get(component.getName()));
				components.put(component.getName(), component);
			}
		}

		return new ScriptMiner().configure(path).configure(components).execute();
	}

	/**
	 * This method saves the result of the analysis mined from the source code of
	 * javascript projects
	 * 
	 * @param dimension         a transverse dimension
	 * @param repository        an instance of {@link Repository} associated with
	 *                          the project being analyized
	 * @param analysisByConcern a table linking each source code artifact, concerns,
	 *                          and an instance of {@link ScriptCodeAnalysis}
	 */
	private static void save(String dimension, Repository repository,
			Table<String, String, ScriptCodeAnalysis> analysisByConcern) {
		DAO.configure(dimension + "_" + repository.getKey().toLowerCase().replaceAll("-", "").replaceAll("\\.", ""));
		// save repository
		RepositoryDAO repoDAO = new RepositoryDAO();
		repoDAO.insert(repository.toDocument());
		// save concerns
		List<Document> concerns = new ArrayList<>();
		for (Map.Entry<String, Map<String, ScriptCodeAnalysis>> row : analysisByConcern.rowMap().entrySet()) {
			String concern = row.getKey();

			Document concernDoc = new Document();
			concernDoc.append("dimension", dimension).append("name", concern);

			List<Document> codeAnalysis = new ArrayList<>();
			for (Map.Entry<String, ScriptCodeAnalysis> column : row.getValue().entrySet()) {
				String file = column.getKey();
				ScriptCodeAnalysis analysis = column.getValue();

				Document analysisDoc = new Document();

				List<Document> metrics = new ArrayList<>();
				metrics.add(new Document().append("name", "LOC").append("value", analysis.getLOC()));

				analysisDoc.append("filename", file).append("imports", analysis.getImports()).append("metrics",
						metrics);

				codeAnalysis.add(analysisDoc);
			}

			concernDoc.append("code_analysis", codeAnalysis);
			concerns.add(concernDoc);
		}

		AksDAO concernDAO = AksDAO.newConcernDAO();
		for (Document concern : concerns) {
			concernDAO.insert(concern);
		}
	}

	/**
	 * Prints this executor's usage
	 */
	private static void printUsage() {
		System.out.println("Architectural Knowledge Suite - Version: " + Configurations.VERSION);
		System.out.println(
				"Usage: java -cp aks.jar mine CONFIGURATION_FILE COMMA_SEPARATED_PATH_TO_PROJECTS COMMA_SEPARATED_PATH_TO_DIMENSIONS org.archknowledge.JavaScriptExecutor");
		System.out.println("Or");
		System.out.println("Usage: java -cp aks.jar export DATASET_PATH COMMA_SEPARATED_PATH_TO_DBS");
	}

	private static String findJsonPackageFile(String path) {
		String file = path + "/package.json";

		if (!FileUtils.fileExists(file)) {
			file = null;
		}

		return file;
	}

	private static Document loadJsonPackage(String file) throws IOException {
		FileInputStream fis = new FileInputStream(new File(file));

		String json = new String(fis.readAllBytes());
		fis.close();

		return Document.parse(json);
	}

	private static Repository initRepository(Document doc) {
		String name = "", description = "";
		if (doc.containsKey("name")) {
			name = doc.getString("name");
		}

		if (doc.containsKey("description")) {
			description = doc.getString("description");
		}

		return new Repository(null, name, name, "", SCMType.GIT, description, new ArrayList<Developer>());
	}

	/**
	 * Extract third-party components from json package files
	 * <p>
	 * Two types of dependencies are supported: dependencies and dev(elopment)
	 * dependencies.
	 * 
	 * @param jsonPackage a json package configuration file from which a set of
	 *                    dependencies must be extracted
	 * @return list of components extracted from the file
	 * @throws IOException
	 */
	private static List<ThirdPartyComponent> fromJSDependenciesToComponents(Document jsonPackage) throws IOException {
		List<ThirdPartyComponent> components = new ArrayList<>();

		components.addAll(fromJSDependenciesToComponents("dependencies", jsonPackage));
		components.addAll(fromJSDependenciesToComponents("devDependencies", jsonPackage));

		return components;
	}

	/**
	 * Extract third-party components from json package files
	 * 
	 * @param typeOfDependency either dependencies or dev(elopment) dependencies
	 * @param jsonPackage      a json package configuration file from which a set of
	 *                         dependencies must be extracted
	 * @return list of components extracted from the file
	 */
	private static List<ThirdPartyComponent> fromJSDependenciesToComponents(String typeOfDependency,
			Document jsonPackage) {
		List<ThirdPartyComponent> components = new ArrayList<>();

		if (jsonPackage.containsKey(typeOfDependency)) {
			Document dependencies = (Document) jsonPackage.get(typeOfDependency);

			Set<java.util.Map.Entry<String, Object>> entries = dependencies.entrySet();
			for (java.util.Map.Entry<String, Object> entry : entries) {
				String name = entry.getKey();
				String version = (String) entry.getValue();
				if (version != null) {
					version = version.replace('^', ' ').replace('~', ' ').trim();
				} else {
					version = "";
				}

				LOG.info("Found component: " + name + ". Version: " + version);

				ThirdPartyComponent component = new ThirdPartyComponent();
				component.setName(name);
				component.setMatch(name);
				component.setImport(name);
				component.setVersion(version);

				components.add(component);
			}
		}

		return components;
	}

	private static void fillTags(ThirdPartyComponent component) throws IOException {
		new PackageMiner().configure(component).mine();
	}

	/**
	 * Saves the list of uncategorized components
	 * <p>
	 * For new projects, most probably, local components configuration files
	 * ("js.components.X.json") will not have metadata about components. After
	 * trying mining the metadata from NPM repository with the help of
	 * {@link PackageMiner}, this method saves the list of uncategorized components
	 * in a new configuration file. The file can be used to insert components'
	 * categories and concerns.
	 * 
	 * @param components list of uncategorized components
	 * @param configPath path in which the new configuration file must be stored
	 * @throws IOException
	 */
	private static void saveUncategorizedComponents(Collection<ThirdPartyComponent> components, String configPath)
			throws IOException {
		List<Document> categories = new ArrayList<>();
		Map<String, Document> concerns = new HashMap<>();

		for (ThirdPartyComponent component : components) {
			Document category = new Document();
			category.append("ids", component.getName());
			category.append("category", component.getTags());
			categories.add(category);

			Document concern = new Document();
			concern.append("id", component.getName());
			concern.append("concern", component.getTags());
			concerns.put(component.getName(), concern);
		}

		String path = new File(configPath).getParentFile().getAbsolutePath();
		path += "/" + org.apache.commons.codec.digest.DigestUtils.sha256Hex(new Date().getTime() + "") + ".json";

		Document doc = new Document();
		doc.append("categories", categories).append("concerns", concerns.values()).append("exclude",
				new ArrayList<String>());

		FileWriter writer = new FileWriter(new File(path));
		writer.write(doc.toJson());
		writer.close();

		LOG.info("Retrieve tags for " + components.size() + " components from NMP. More info in " + path);
	}

	private static Document loadConfiguration(String configFile) throws IOException {
		Document config = null;

		File file = new File(configFile);
		if (file.exists()) {
			String json = "";
			try (BufferedReader reader = Files.newBufferedReader(Paths.get(configFile))) {
				String line = null;
				while ((line = reader.readLine()) != null) {
					json += line;
				}
			}

			if (!json.isEmpty()) {
				config = Document.parse(json);
			}
		}

		return config;
	}

	@SuppressWarnings("unchecked")
	private static Map<String, String> loadConcerns(Document config) {
		Map<String, String> concerns = new HashMap<>();

		// load the list of concerns
		for (Document concern : (List<Document>) config.get("concerns")) {
			concerns.put(concern.getString("id"), concern.getString("concern"));
		}

		return concerns;
	}

}
