package org.archknowledge;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.archknowledge.config.ConfigurationException;
import org.archknowledge.config.Configurations;
import org.archknowledge.export.Exporter;
import org.archknowledge.export.SourceCodeExporter;
import org.archknowledge.export.exporters.ComplexityBySmellExporter;
import org.archknowledge.export.exporters.ComplexityExporter;
import org.archknowledge.export.exporters.DedicationByCodeSmellExporter;
import org.archknowledge.export.exporters.DedicationExporter;
import org.archknowledge.mining.ArchKnowledgeMiner;
import org.archknowledge.mining.MavenMiner;
import org.archknowledge.mining.Miner;
import org.archknowledge.model.ComponentsConfig;
import org.archknowledge.model.Knowledge;
import org.archknowledge.model.ThirdPartyComponent;
import org.bson.Document;
import org.repositoryminer.metrics.codesmell.CodeSmellId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Executor for mining Java Projects
 * <p>
 * {@link JavaExecutor} supports the following commands:
 * <p>
 * <ul>
 * <li>Mining - activates the mining of concerns. The final dataset is stored as
 * mongodb's collections (see {@link ArchKnowledgeMiner})</li>
 * <li>Export - exports the data contained from the database to CSV files</li>
 * </ul>
 * <p>
 * The mining command must be complemented by providing:
 * <p>
 * <ul>
 * <li>A "mining.config" file to parameterize the execution of
 * {@link ArchKnowledgeMiner}</li>
 * <li>A set of "components.X.json" files to fill information about
 * missing/excluded categories of components</li>
 * </ul>
 * The export command line requires and "export.config" to parameterize
 * instances of {@link Exporter} and specialized exporters, e.g.,
 * {@link ComplexityExporter}.
 * 
 * @author Luis Paulo
 */
public class JavaExecutor {

	private static final Logger LOG = LoggerFactory.getLogger(JavaExecutor.class);

	/**
	 * Main method of the executor. It activates the mining/export of projects or
	 * print the usage if it fails to parse a valid command line.
	 * 
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		int len = args.length;

		if (len > 0) {
			String param = args[0];

			if (param.startsWith("-mining.config=")) {
				String configPath = param.substring("-mining.config=".length());

				mine(configPath, args, false);
			} else if (param.startsWith("-inspect.config=")) {
				String configPath = param.substring("-inspect.config=".length());

				mine(configPath, args, true);
			} else if (param.startsWith("-export.config=")) {
				export(param);
			} else {
				printUsage();
			}
		} else {
			printUsage();
		}

	}

	/**
	 * Run mining routines contained in each instance of {@link Miner}
	 * <p>
	 * It is dependent on valid mining and components configuration files. If the
	 * files are not valid a configuration exception is thrown.
	 * 
	 * @param configPath     a valid "mining.config" file
	 * @param componentsFile a list of valid "components.config" files
	 * @throws Exception
	 */
	private static void mine(String configPath, String[] args, boolean inspect) throws Exception {
		Document config = loadConfigurations(configPath);
		if (config == null) {
			throwException("Mining configuration is empty or invalid!");
		} else {
			List<Document> componentsConfig = loadMultipleConfigurations(config);

			List<ArchKnowledgeMiner> miners = configureMiners(config, configureComponents(componentsConfig));
			if ((miners == null) || (miners.isEmpty())) {
				throwException("Unable to configure miners!");
			} else {
				Set<ThirdPartyComponent> uncategorizedComponents = new HashSet<>();

				LOG.info("Running miners now...");
				for (ArchKnowledgeMiner miner : miners) {
					miner.mine(inspect);
					uncategorizedComponents.addAll(miner.getUncategorizedComponents());
				}

				saveUncategorizedComponents(uncategorizedComponents, configPath);

				LOG.info("Done!");
			}
		}
	}

	/**
	 * Run export routines contained in each instance of {@link Exporter}
	 * <p>
	 * It is dependent on valid export configuration file. If the file is not valid
	 * a configuration exception is thrown.
	 * 
	 * @param configFile a valid "export.config" file
	 * @throws Exception
	 */
	private static void export(String configFile) throws Exception {
		Document config = loadConfigurations(configFile.substring("-export.config=".length()));
		if (config == null) {
			throwException("Export configuration is empty or invalid!");
		} else {
			Map<Class<?>, List<Exporter>> map = configureExporters(config);
			if ((map == null) || (map.isEmpty())) {
				throwException("Unable to configure exporters!");
			} else {
				LOG.info("Running exporters now...");

				// let's remove all files from any previous exporting
				deleteSourceCodeFiles(map.values());

				for (Class<?> clazz : map.keySet()) {
					List<String> allLines = new ArrayList<>();

					Exporter exporter = null;
					Iterator<Exporter> i = map.get(clazz).iterator();
					while (i.hasNext()) {
						exporter = i.next();

						allLines.addAll(exporter.export().getLines());
					}

					exporter.setFileName("all").export(allLines);

					// dataset may comprise several source code artifacts (let's compress
					// the files and delete the original files)
					new SourceCodeExporter().configure(exporter.getFilePath()).compress(true);
				}
			}
		}
	}

	/**
	 * remove all source code file before new exports
	 * 
	 * @param collection
	 */
	private static void deleteSourceCodeFiles(Collection<List<Exporter>> mappedExporters) {
		Set<String> outputPaths = new HashSet<>();

		for (List<Exporter> exporters : mappedExporters) {
			for (Exporter exporter : exporters) {
				outputPaths.add(exporter.getFilePath());
			}
		}

		for (String path : outputPaths) {
			new SourceCodeExporter().configure(path).deleteSourceFiles();
		}

	}

	/**
	 * Prints this executor's usage
	 */
	private static void printUsage() {
		System.out.println("Architectural Knowledge Suite - Version: " + Configurations.VERSION);
		System.out.println("Usage: java -jar aks.jar -mining.config=PATH_TO_CONFIGURATION_FILE");
		System.out.println("Or");
		System.out.println("Usage: java -jar aks.jar -inspect.config=PATH_TO_CONFIGURATION_FILE");
		System.out.println("Or");
		System.out.println("Usage: java -jar aks.jar -export.config=PATH_TO_CONFIGURATION_FILE");
	}

	/**
	 * Save uncategorized components
	 * <p>
	 * {@link MavenMiner} can be used to find categories for components when they
	 * are not found in the components config file. Ideally, the missing components
	 * must be added to this file to avoid contacting MVNRepository as much as
	 * possible. Doing this accelerates the mining of projects.
	 * 
	 * @param uncategorizedComponents a set of uncategorized components
	 * @param configPath              path to new config file containing
	 *                                uncategorized components
	 * @throws IOException
	 */
	private static void saveUncategorizedComponents(Set<ThirdPartyComponent> uncategorizedComponents, String configPath)
			throws IOException {
		List<Document> categories = new ArrayList<>();
		Map<String, Document> concerns = new HashMap<>();

		for (ThirdPartyComponent component : uncategorizedComponents) {
			Document category = new Document();
			category.append("ids", component.getKey());
			category.append("category", component.getCategory());
			categories.add(category);

			Document concern = new Document();
			concern.append("id", component.getImport());
			concern.append("concern", "undefined");
			concerns.put(component.getImport(), concern);
		}

		if (!categories.isEmpty()) {
			Document doc = new Document();
			doc.append("categories", categories).append("concerns", concerns.values()).append("exclude",
					new ArrayList<String>());

			String path = new File(configPath).getParentFile().getAbsolutePath();
			path += "/" + org.apache.commons.codec.digest.DigestUtils.sha256Hex(new Date().getTime() + "") + ".json";

			FileWriter writer = new FileWriter(new File(path));
			writer.write(doc.toJson());
			writer.close();

			LOG.info("Tried to retrieve categories for " + categories.size()
					+ " components from MVNRepository. More info in " + path);
		}
	}

	/**
	 * Parse multiple configuration files as json documents
	 * <p>
	 * Configuration files are stored in JSON files. Multiple configurations can be
	 * loaded at once by inputing a string in which configs are separated by "|"
	 * 
	 * @param config
	 * @return
	 * @throws IOException
	 */
	private static List<Document> loadMultipleConfigurations(Document config) throws IOException {
		List<Document> configs = new ArrayList<>();

		@SuppressWarnings("unchecked")
		List<String> files = (ArrayList<String>) config.get("components_config");
		for (String file : files) {
			configs.add(loadConfigurations(file));
		}

		return configs;
	}

	/**
	 * Parse a configuration file as a JSON document
	 * <p>
	 * All configurations are stored in JSON files, so we have provided this generic
	 * method to load config files into instances of JSON documents.
	 * 
	 * @param configFile a path to a valid JSON-based configuration file
	 * @return an instance of Document
	 * @throws IOException
	 */
	private static Document loadConfigurations(String configFile) throws IOException {
		Document config = null;

		File file = new File(configFile);
		if (file.exists()) {
			// read all content from the config file
			String json = "";
			try (BufferedReader reader = Files.newBufferedReader(Paths.get(configFile))) {
				String line = null;
				while ((line = reader.readLine()) != null) {
					json += line;
				}
			}

			if (!json.isEmpty()) {
				config = Document.parse(json);
			}
		}

		return config;
	}

	/**
	 * Configure all mining plugins
	 * <p>
	 * After parsing a valid "mining.config" file as a Document instance each needed
	 * configuration is injected in instances of {@link ArchKnowledgeMiner}.
	 * <p>
	 * An instance of {@link ArchKnowledgeMiner} is created for each dimension
	 * informed in the configuration file.
	 * 
	 * @param config           a mining configuration document
	 * @param componentsConfig an instance of {@link ComponentsConfig}
	 * @return list of configured instances of {@link ArchKnowledgeMiner}
	 * @throws ConfigurationException
	 */
	@SuppressWarnings("unchecked")
	private static List<ArchKnowledgeMiner> configureMiners(Document config, ComponentsConfig componentsConfig)
			throws ConfigurationException {
		List<ArchKnowledgeMiner> miners = new ArrayList<ArchKnowledgeMiner>();

		configureMongo(config);

		List<Document> dimensions = (List<Document>) config.get("dimensions");
		for (Document dimension : dimensions) {
			List<Document> projects = (List<Document>) dimension.get("projects");
			for (Document project : projects) {
				miners.add(new ArchKnowledgeMiner().configure(dimension.getString("name")).configure(project)
						.configure(componentsConfig)
						.configure(Knowledge.toMiners((List<String>) config.get("knowledges"))));
			}
		}

		return miners;
	}

	/**
	 * Configure all exporters
	 * <p>
	 * After parsing a valid "export.config" file as a Document instance each needed
	 * configuration is injected in instances of {@link Exporter}.
	 * <p>
	 * An instance of {@link Exporter} is created for each dimension informed in the
	 * configuration file.
	 * 
	 * @param config
	 * @return a map which associates instances of {@link Exporter} to each class
	 *         type that implements specific export routines
	 * @throws ConfigurationException
	 */
	@SuppressWarnings("unchecked")
	private static Map<Class<?>, List<Exporter>> configureExporters(Document config) throws ConfigurationException {
		Map<Class<?>, List<Exporter>> map = initExportersMap();

		configureMongo(config);

		boolean processEmptyMethods = config.getBoolean("process_empty_methods", true);
		if (!processEmptyMethods) {
			LOG.info("Empty methods will not be processed. So, interfaces may not be processed either!");
		}

		String transverseDimension = config.getString("transverse_dimension");
		LOG.info("Configuring exporters for " + transverseDimension + "...");

		List<Document> dimensions = (List<Document>) config.get("dimensions");
		for (Document dimension : dimensions) {
			List<String> projects = (List<String>) dimension.get("projects");
			for (String project : projects) {
				// dedication
				map.get(DedicationExporter.class)
						.add(newDedicationExporter(dimension.getString("name"), project, processEmptyMethods, config));
				map.get(DedicationByCodeSmellExporter.class).add(newDedicationByCodeSmellExporter(
						dimension.getString("name"), project, processEmptyMethods, config));
				// complexity
				map.get(ComplexityExporter.class)
						.add(newComplexityExporter(dimension.getString("name"), project, config));
				map.get(ComplexityBySmellExporter.class)
						.add(newComplexityBySmellExporter(dimension.getString("name"), project, config));
			}
		}

		return map;
	}

	private static Map<Class<?>, List<Exporter>> initExportersMap() {
		Map<Class<?>, List<Exporter>> map = new HashMap<>();

		map.put(DedicationExporter.class, new ArrayList<>());
		map.put(DedicationByCodeSmellExporter.class, new ArrayList<>());
		map.put(ComplexityExporter.class, new ArrayList<>());
		map.put(ComplexityBySmellExporter.class, new ArrayList<>());

		return map;
	}

	private static DedicationExporter newDedicationExporter(String dimension, String project,
			boolean processEmptyMethods, Document config) {
		DedicationExporter exporter = new DedicationExporter();

		String key = (dimension + "_" + project).toLowerCase();

		config = (Document) config.get("dedication_exporter");
		exporter.configure(project.toLowerCase(), config.getString("output_path"), key, key);
		exporter.configure(processEmptyMethods, config.getBoolean("use_ICD", false));

		return exporter;
	}

	private static DedicationByCodeSmellExporter newDedicationByCodeSmellExporter(String dimension, String project,
			boolean processEmptyMethods, Document config) {
		DedicationByCodeSmellExporter exporter = new DedicationByCodeSmellExporter();

		String key = (dimension + "_" + project).toLowerCase();

		config = (Document) config.get("dedication_by_smell_exporter");
		exporter.configure(project.toLowerCase(), config.getString("output_path"), key, key);

		return exporter;
	}

	private static ComplexityExporter newComplexityExporter(String dimension, String project, Document config) {
		ComplexityExporter exporter = new ComplexityExporter();

		String key = (dimension + "_" + project).toLowerCase();

		config = (Document) config.get("complexity_exporter");
		exporter.configure(project.toLowerCase(), config.getString("output_path"), key, key);

		return exporter;
	}

	private static ComplexityBySmellExporter newComplexityBySmellExporter(String dimension, String project,
			Document config) {
		ComplexityBySmellExporter exporter = new ComplexityBySmellExporter();

		String key = (dimension + "_" + project).toLowerCase();

		config = (Document) config.get("complexity_by_smell_exporter");
		exporter.configure(project.toLowerCase(), config.getString("output_path"), key, key);
		exporter.configure(getSmells(config));

		return exporter;
	}

	@SuppressWarnings("unchecked")
	private static List<CodeSmellId> getSmells(Document config) {
		List<CodeSmellId> smells = new ArrayList<>();

		List<String> zmells = (List<String>) config.get("code_smells");
		for (String zmell : zmells) {
			smells.add(CodeSmellId.valueOf(zmell));
		}

		return smells;
	}

	private static void configureMongo(Document config) throws ConfigurationException {
		// configure mongodb
		String uri = config.getString("mongodb_uri");
		if ((uri == null) || (uri.isEmpty())) {
			throwException("Value of 'mongodb_uri' should not be empty!");
		}
		Configurations.MONGODB_URI = uri;
	}

	@SuppressWarnings("unchecked")
	private static ComponentsConfig configureComponents(List<Document> configs) {
		ComponentsConfig componentsConfig = new ComponentsConfig();

		if (!configs.isEmpty()) {
			for (Document config : configs) {
				// load the list of categories
				List<Document> categoriesConfig = (List<Document>) config.get("categories");
				for (Document c : categoriesConfig) {
					componentsConfig.getCategories().put(c.getString("ids"), c.getString("category"));
				}
				// load the list of concerns
				List<Document> concernsConfig = (List<Document>) config.get("concerns");
				for (Document c : concernsConfig) {
					Set<String> concerns = new HashSet<>();
					if (componentsConfig.getConcerns().containsKey(c.getString("id"))) {
						concerns = componentsConfig.getConcerns().get(c.getString("id"));
					} else {
						componentsConfig.getConcerns().put(c.getString("id"), concerns);
					}

					concerns.add(c.getString("concern"));
				}
				// load the list of excluded components
				List<String> excludedsConfig = (List<String>) config.get("exclude");
				for (String c : excludedsConfig) {
					componentsConfig.getExcluded().add(c);
				}
			}
		}

		return copyComponents(componentsConfig, configs);
	}

	@SuppressWarnings("unchecked")
	private static ComponentsConfig copyComponents(ComponentsConfig componentsConfig, List<Document> configs) {
		for (Document config : configs) {
			List<Document> copiesConfig = (List<Document>) config.get("copy");

			for (Document c : copiesConfig) {
				List<String> ids = (List<String>) c.get("ids");
				String to = c.getString("to");

				for (String id : ids) {
					Set<String> concerns = componentsConfig.getConcerns().get(id);
					concerns.add(to);
				}
			}
		}

		return componentsConfig;
	}

	private static void throwException(String message) throws ConfigurationException {
		throw new ConfigurationException(message);
	}

}
