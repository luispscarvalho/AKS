package org.archknowledge.config;

/**
 * Some hard-coded/static configurations. TODO add an external config file (?)
 * 
 * @author Luis Paulo
 */
public abstract class Configurations {

	public static final String VERSION = "3.0";

	// Persistence configurations
	public static String MONGODB_URI = "mongodb://localhost";

	// MavenMiner configurations
	public static final String MAVEN_REPO_URL = "https://mvnrepository.com/artifact/";
	public static final int MAVEN_REPOSITORY_ACCESS_INTERVAL = 15; // seconds

}
