package org.archknowledge.config;

/**
 * A generic configuration exception
 * <p>
 * The exception must be thrown whenever a configuration step does not execute
 * as intended, e.g., a configuration file was not found.
 * 
 * @author Luis Paulo
 */
public class ConfigurationException extends Exception {

	private static final long serialVersionUID = 4020511519191427835L;

	public ConfigurationException(String message) {
		super("Configuration erro: " + message);
	}
}
