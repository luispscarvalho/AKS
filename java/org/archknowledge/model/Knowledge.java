package org.archknowledge.model;

import java.util.ArrayList;
import java.util.List;

import org.archknowledge.JavaExecutor;
import org.archknowledge.mining.ArchKnowledgeMiner;
import org.archknowledge.mining.Miner;
import org.archknowledge.mining.miners.ComplexityMiner;
import org.archknowledge.mining.miners.DedicationMiner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * List of all knowledges AKS is capable of mining
 * <p>
 * It is used by {@link ArchKnowledgeMiner} to configure miners. The list of
 * knowledges must be added to mining config files prior to being processed by
 * {@link JavaExecutor}.
 * 
 * @author Luis Paulo
 */
public enum Knowledge {

	DEDICATION, COMPLEXITY;

	private static final Logger LOG = LoggerFactory.getLogger(Knowledge.class);

	public static List<Miner> toMiners(List<String> knowledges) {
		List<Miner> miners = new ArrayList<>();

		for (String knowledge : knowledges) {
			miners.addAll(toMiners(Knowledge.valueOf(knowledge)));
		}

		return miners;
	}

	public static List<Miner> toMiners(Knowledge knowledge) {
		List<Miner> miners = new ArrayList<>();

		if (knowledge.equals(DEDICATION)) {
			miners.add(new DedicationMiner());
		} else if (knowledge.equals(COMPLEXITY)) {
			miners.add(new ComplexityMiner());
		} else {
			LOG.error("No miner for knowledge: " + knowledge);
		}

		return miners;
	}
}
