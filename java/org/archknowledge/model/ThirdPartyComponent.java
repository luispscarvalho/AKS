package org.archknowledge.model;

import java.util.Set;

import org.archknowledge.mining.miners.ThirdPartyComponentsMiner;

/**
 * This class encapsulates information about third-party components
 * <p>
 * It is used by other classes to store and exchange information about software
 * projects' injected components (or dependencies). For instance,
 * {@link ThirdPartyComponentsMiner} fills instances of this class with metadata
 * obtained from pom and gradle files.
 * 
 * @author Luis Paulo
 */
public class ThirdPartyComponent {

	private String name;
	private String category;
	private String version;
	private String concern;
	private Set<String> concerns;
	private Set<String> tags;

	private String impoRt;
	private String match;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImport() {
		return impoRt;
	}

	public void setImport(String impoRt) {
		this.impoRt = impoRt;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getMatch() {
		return match;
	}

	public void setMatch(String match) {
		this.match = match;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getConcern() {
		return concern;
	}

	public void setConcern(String concern) {
		this.concern = concern;
	}

	public Set<String> getConcerns() {
		return concerns;
	}

	public void setConcerns(Set<String> concerns) {
		this.concerns = concerns;
	}

	public String getKey() {
		return impoRt + ":" + name;
	}

	public Set<String> getTags() {
		return tags;
	}

	public void setTags(Set<String> tags) {
		this.tags = tags;
	}

	@Override
	public String toString() {
		return getKey() + "[Category/Match: " + (category == null ? "" : category) + "/" + (match == null ? "" : match)
				+ "]";
	}

	@Override
	public boolean equals(Object other) {
		ThirdPartyComponent otherComponent = (ThirdPartyComponent) other;

		return getKey().equals(otherComponent.getKey());
	}

	@Override
	public int hashCode() {
		return getKey().hashCode();
	}

}
