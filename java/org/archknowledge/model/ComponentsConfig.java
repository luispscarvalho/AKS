package org.archknowledge.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.archknowledge.JavaExecutor;

/**
 * A container for categories of components and their respective concerns
 * <p>
 * The {@link JavaExecutor} uses this class to inject information extracted from
 * each "components.X.json" configuration file. As not all components have been
 * categorized by MVNRepository, this class can be used as a catalog for all
 * missing components categories and concerns.
 * <p>
 * It can also be used to inform IDs of components which must not be analyzed by the
 * miners.
 * 
 * @author Luis Paulo
 */
public class ComponentsConfig {

	private List<String> excluded;
	private Map<String, String> categories;
	private Map<String, Set<String>> concerns;

	public ComponentsConfig() {
		excluded = new ArrayList<>();
		categories = new HashMap<>();
		concerns = new HashMap<>();

	}

	public List<String> getExcluded() {
		return excluded;
	}

	public void setExcluded(List<String> excluded) {
		this.excluded = excluded;
	}

	public Map<String, String> getCategories() {
		return categories;
	}

	public void setCategories(Map<String, String> categories) {
		this.categories = categories;
	}

	public Map<String, Set<String>> getConcerns() {
		return concerns;
	}

	public void setConcerns(Map<String, Set<String>> concerns) {
		this.concerns = concerns;
	}

}
