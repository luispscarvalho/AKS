package org.archknowledge.persistence;

import org.archknowledge.config.Configurations;
import org.archknowledge.mining.ArchKnowledgeMiner;
import org.repositoryminer.persistence.GenericDAO;
import org.repositoryminer.persistence.MongoConnection;

/**
 * Our generic DAO class
 * <p>
 * This class has the purpose to allow a previous configuration of database
 * access via the {@link #configure(String)} method. A valid URI must be
 * provided, so that the DAO can locate and contact the database.
 * {@link ArchKnowledgeMiner} call {@link #configure(String)} prior to mining
 * each project.
 * <p>
 * As well, it is necessary to define the name of the collection which will be
 * used to insert/retrieve data.
 * 
 * @author Luis Paulo
 */
public class DAO extends GenericDAO {

	public DAO(String collectionName) {
		super(collectionName);
	}

	public static void configure(String key) {
		MongoConnection conn = MongoConnection.getInstance();
		conn.connect(Configurations.MONGODB_URI, key);
	}
	
	public static void configure(String URI, String key) {
		MongoConnection conn = MongoConnection.getInstance();
		conn.connect(URI, key);
	}

}
