package org.archknowledge.persistence;

import org.bson.Document;
import org.bson.types.ObjectId;
import org.repositoryminer.metrics.persistence.CodeAnalysisDAO;

import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Projections;

/**
 * Utility class to provide access to database collections
 * 
 * @author luisp
 */
public class StaticDAO {

	public static Document getCodeAnalysis(ObjectId analysisId) {
		CodeAnalysisDAO dao = new CodeAnalysisDAO();
		Document analysis = dao.findById(analysisId,
				Projections.include("source", "filename", "imports", "metrics", "classes"));

		return analysis;
	}

	public static Document getFrame(String reference) {
		AksDAO dao = AksDAO.newFrameDAO();

		return dao.findOne(Filters.eq("reference", reference), null);
	}

}
