package org.archknowledge.persistence;

import org.repositoryminer.persistence.GenericDAO;

/**
 * A factory for AKS-related DAOs
 * 
 * @author Luis Paulo
 */
public class AksDAO extends GenericDAO {

	public static AksDAO newCategoryDAO() {
		AksDAO dao = new AksDAO("aks_category");

		return dao;
	}

	public static AksDAO newFrameDAO() {
		AksDAO dao = new AksDAO("aks_frame");
	
		return dao;
	}

	public static AksDAO newConcernDAO() {
		AksDAO dao = new AksDAO("aks_concern");

		return dao;
	}

	public static AksDAO newDedicationDAO() {
		AksDAO dao = new AksDAO("aks_dedication");

		return dao;
	}

	public static AksDAO newCodeSmellDAO() {
		AksDAO dao = new AksDAO("aks_codesmell");

		return dao;
	}

	public static AksDAO newComplexityDAO() {
		AksDAO dao = new AksDAO("aks_complexity");

		return dao;
	}

	public static AksDAO newDedicationByFileDAO() {
		AksDAO dao = new AksDAO("aks_dedication_by_file");

		return dao;
	}

	private AksDAO(String collectionName) {
		super(collectionName);
	}

}
