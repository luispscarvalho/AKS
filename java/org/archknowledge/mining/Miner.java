package org.archknowledge.mining;

import org.archknowledge.JavaExecutor;
import org.archknowledge.model.ComponentsConfig;
import org.repositoryminer.metrics.RepositoryMinerMetrics;

/**
 * A template for all miners
 * <p>
 * It determines how miners must be configured and executed by
 * {@link ArchKnowledgeMiner}. Any new miner must comply with the methods and
 * parameters defined below.
 * 
 * @author Luis Paulo
 */
public interface Miner {

	/**
	 * Inject an instance of {@link RepositoryMinerMetrics} in the miner
	 * <p>
	 * Miners might want to access some of RM's {@link RepositoryMinerMetrics}
	 * parameters to perform mining routines
	 * 
	 * @param metricsMiner instance of {@link RepositoryMinerMetrics}
	 * @return instance of this (Miner)
	 */
	public Miner configure(RepositoryMinerMetrics metricsMiner);

	/**
	 * Insert an instance of {@link ComponentsConfig} in the miner
	 * <p>
	 * Not all components have been categorized by maven repository, so an instance
	 * of {@link ComponentsConfig} might be needed to to fill uncategorized
	 * compoonents/concerns in the dataset. {@link ComponentsConfig}. is also
	 * responsible for determining the components which must be excluded from
	 * mining/analysis
	 * 
	 * @param componentsConfig instance of {@link ComponentsConfig}
	 * @return instance of this (Miner)
	 */
	public Miner configure(ComponentsConfig componentsConfig);

	/**
	 * Determine the name of the transverse dimension being mined
	 * <p>
	 * The dimension can be specified in "mining.json" configuration file injected
	 * in AKS via {@link JavaExecutor}.
	 * 
	 * @param dimension a transverse dimension used as analysis context
	 * @return instance of this (Miner)
	 */
	public Miner configure(String dimension);

	/**
	 * Warning the miner whether a inspection is being carried
	 * 
	 * @param inspecting
	 * @return instance of this (Miner)
	 */
	public Miner configure(boolean inspecting);

	/**
	 * Execute mining routines
	 * <p>
	 * Succeeding the configuration of the miner, {@link ArchKnowledgeMiner} calls
	 * this method to perform mining routines.
	 * 
	 * @param reference the current reference being mined. It can be used to
	 *                  determine the current reference
	 * @return instance of this (Miner)
	 * @throws Exception
	 */
	public Miner execute(String reference) throws Exception;

	/**
	 * Execute routines after the mining
	 * <p>
	 * It can be used to execute further mining/analysis routines after the mining
	 * is performed.
	 * 
	 * @return instance of this (Miner)
	 * @throws Exception
	 */
	public Miner postExecute() throws Exception;

	/**
	 * @return the name of the miner
	 */
	public String getName();

}
