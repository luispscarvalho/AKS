package org.archknowledge.mining.miners;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.archknowledge.mining.Miner;
import org.archknowledge.model.ComponentsConfig;
import org.archknowledge.persistence.AksDAO;
import org.archknowledge.persistence.StaticDAO;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.repositoryminer.metrics.RepositoryMinerMetrics;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Measure the code complexity of source code artifacts that are associated with
 * concerns
 * <p>
 * It includes the measurement of two complexity-related metrics: (i) WMC - the
 * Weighted Methods per Class metric; and (ii) AMW - the Average Method Weight.
 * <p>
 * The mining strategy also saves information about instances of code smells
 * found in the artifacts. This can be used as a filter to find only the cases
 * in which the complexity is associated with smells.
 * 
 * @author Luis Paulo
 */
public class ComplexityMiner implements Miner {

	private static final Logger LOG = LoggerFactory.getLogger(ComplexityMiner.class);

	private RepositoryMinerMetrics metricsMiner;
	private boolean inspecting;
	private String dimension;

	private List<String> references;

	public ComplexityMiner() {
		references = new ArrayList<>();
	}

	@Override
	public Miner configure(RepositoryMinerMetrics metricsMiner) {
		this.metricsMiner = metricsMiner;

		return this;
	}

	@Override
	public Miner configure(ComponentsConfig componentsConfig) {
		return this;
	}

	@Override
	public Miner configure(String dimension) {
		this.dimension = dimension;

		return this;
	}

	@Override
	public Miner configure(boolean inspecting) {
		this.inspecting = inspecting;

		return this;
	}

	@Override
	public Miner execute(String reference) throws Exception {
		references.add(reference);

		return this;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Miner postExecute() throws Exception {
		if (!inspecting) {
			List<Document> concerns = AksDAO.newConcernDAO().findMany(null, null);

			// for each concern...
			for (Document concern : concerns) {
				String name = concern.getString("name");
				LOG.info("Mining complexities for concern '" + name + "'");

				List<Document> references = (List<Document>) concern.get("references");
				// and for each reference affecting the concern...
				for (Document reference : references) {
					List<Document> analysises = (List<Document>) reference.get("code_analysis");
					// ...let's calculate the complexity associate to each code analysis...
					for (Document analysis : analysises) {
						ObjectId analysisId = analysis.getObjectId("id");
						Document codeAnalysis = StaticDAO.getCodeAnalysis(analysisId);

						Document complexity = new Document();
						complexity.append("repository", metricsMiner.getRepositoryId())
								.append("dimension", this.dimension)
								.append("reference", reference.getString("reference"))
								.append("filename", codeAnalysis.getString("filename"))
								.append("source", codeAnalysis.getString("source"))
								.append("concern", concern.getString("name")).append("imports", analysis.get("imports"))
								.append("complexities", getComplexities(codeAnalysis));

						AksDAO.newComplexityDAO().insert(complexity);
					}
				}

			}

			LOG.info("Mining of complexities ended!");
		}

		return this;
	}

	@SuppressWarnings("unchecked")
	private List<Document> getComplexities(Document analysis) throws IOException {
		List<Document> complexities = new ArrayList<>();

		List<Document> classes = (List<Document>) analysis.get("classes");
		for (Document clazz : classes) {
			Document complexity = new Document();
			complexity.append("class", clazz.getString("name"));
			complexity.append("codesmells", clazz.get("codesmells"));

			List<Document> metrics = (List<Document>) clazz.get("metrics");
			Integer wmc = getMetricValue(metrics, "WMC");
			complexity.append("WMC", wmc);
			Double amw = getMetricValue(metrics, "AMW");
			complexity.append("AMW", amw);

			complexities.add(complexity);
		}

		return complexities;
	}

	@SuppressWarnings("unchecked")
	private <T> T getMetricValue(List<Document> metrics, String name) {
		T value = null;

		for (Document metric : metrics) {
			if (name.equals(metric.getString("name"))) {
				value = (T) metric.get("value");

				break;
			}
		}

		return value;
	}

	@Override
	public String getName() {
		return "Code Complexity Miner";
	}

}
