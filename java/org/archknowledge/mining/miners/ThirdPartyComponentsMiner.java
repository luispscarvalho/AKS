package org.archknowledge.mining.miners;

import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.StringTokenizer;

import org.apache.maven.model.Dependency;
import org.apache.maven.model.DependencyManagement;
import org.apache.maven.model.Model;
import org.apache.maven.model.io.xpp3.MavenXpp3Reader;
import org.archknowledge.mining.MavenMiner;
import org.archknowledge.mining.Miner;
import org.archknowledge.mining.UncategorizedComponentListener;
import org.archknowledge.model.ComponentsConfig;
import org.archknowledge.model.ThirdPartyComponent;
import org.archknowledge.persistence.AksDAO;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.codehaus.plexus.util.xml.pull.XmlPullParserException;
import org.repositoryminer.metrics.RepositoryMinerMetrics;
import org.repositoryminer.metrics.persistence.CodeAnalysisDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Projections;

/**
 * Extract concern-related information from third-party components' metadata
 * <p>
 * Extraction of components is based on the parsing and processing of pom/gradle
 * files that are used to inject dependencies in software projects.
 * <p>
 * We rely on "org.apache.maven" to parse components from pom files. As we did
 * not found any parser for gradle files, we have added our own routines to
 * parse gradle components. So, the parsing/extraction of gradle components can
 * be faulty because of the lack of an adequate parser. We recommend favoring
 * the analysis of software projects that rely on pom files to automate the
 * injection of components.
 * <p>
 * Categorization of components is based on the categories of components
 * provided by MVNRepository website. One important remark: not all components
 * have been categorized by MVNRepository. As a consequence, the categorization
 * must be complemented by manually filling missing categories and concerns.
 * 
 * @author Luis Paulo
 */
public class ThirdPartyComponentsMiner implements Miner {

	private static final Logger LOG = LoggerFactory.getLogger(ThirdPartyComponentsMiner.class);

	// a collection of (gradle) dependency tokens
	private static class DependencyToken {
		private String tokenBegin, tokenEnd;

		public DependencyToken(String tokenBegin, String tokenEnd) {
			this.tokenBegin = tokenBegin;
			this.tokenEnd = tokenEnd;
		}

		public String getTokenBegin() {
			return tokenBegin;
		}

		public String getTokenEnd() {
			return tokenEnd;
		}
	}

	// tokens for injecting information about components/dependencies in gradle
	// files
	private static final DependencyToken[] GRADLE_DEPENDENCY_TOKENS = new DependencyToken[] {
			new DependencyToken("dependency(\"", "\")"), new DependencyToken("dependency('", "')"),
			new DependencyToken("implementation \"", "\""), new DependencyToken("implementation '", "'"),
			new DependencyToken("compile \"", "\""), new DependencyToken("compile '", "'"),
			new DependencyToken("compile('", "')") };

	private List<ThirdPartyComponent> thirdPartyComponents = new ArrayList<>();
	private String dimension;

	private ComponentsConfig componentsConfig;
	private RepositoryMinerMetrics metricsMiner;
	private MavenMiner mavenMiner;

	/**
	 * Configure a new maven repository miner
	 * <p>
	 * Components config file may fail to retrieve information about components.
	 * Thus, if needed, an instance of {@link UncategorizedComponentListener} can be
	 * injected in {@link MavenMiner} to keep track of missing components
	 * 
	 * @param listener
	 * @return instance of this (ThirdPartyComponentsMiner)
	 */
	public ThirdPartyComponentsMiner configure(UncategorizedComponentListener listener) {
		mavenMiner = new MavenMiner().configure(listener);

		return this;
	}

	@Override
	public Miner configure(RepositoryMinerMetrics metricsMiner) {
		this.metricsMiner = metricsMiner;

		return this;
	}

	@Override
	public Miner configure(ComponentsConfig componentsConfig) {
		this.componentsConfig = componentsConfig;

		return this;
	}

	@Override
	public Miner configure(String dimension) {
		this.dimension = dimension;

		return this;
	}

	@Override
	public Miner configure(boolean inspecting) {
		return this;
	}

	@Override
	public Miner execute(String reference) throws Exception {
		LOG.info("Mining components for reference '" + reference + "' from path: " + metricsMiner.getCheckoutPath());

		List<String> paths = getAllPaths();

		appendNewComponents(extractComponentsFromPom(paths, "pom.xml"));
		appendNewComponents(extractComponentsFromPom(paths, "pom.xml.in"));
		appendNewComponents(extractComponentsFromGradle(paths, "build.gradle"));

		return this;
	}

	@Override
	public Miner postExecute() throws Exception {
		LOG.info("Found " + thirdPartyComponents.size() + " third-party components");

		excludeUndesiredComponents().fillCategory().fillConcern().saveCategories().saveConcerns();

		return this;
	}

	/**
	 * Find all subpaths under the checkout path
	 * <p>
	 * This is needed to reach pom files stored in projects' subfolders
	 * 
	 * @return a list of paths right under the checkout path
	 */
	private List<String> getAllPaths() {
		List<String> paths = new ArrayList<String>();

		String root = metricsMiner.getCheckoutPath() + "/";
		paths.add(root);

		File[] subPaths = new File(root).listFiles(new FileFilter() {
			@Override
			public boolean accept(File file) {
				return file.isDirectory();
			}
		});

		LOG.info("Found " + subPaths.length + " subpaths in :" + metricsMiner.getCheckoutPath());
		for (File path : subPaths) {
			paths.add(path.getAbsolutePath() + "/");
		}

		return paths;
	}

	/**
	 * Extract components from a list of .gradle files
	 * <p>
	 * It tries to find a valid gradle file stored in a list of paths prior to
	 * parsing it. A necessary step as some projects embed gradle files in
	 * subfolders
	 * 
	 * @param paths      list of paths where gradle files might be stored in
	 * @param gradleFile name of a gradle file
	 * @return a list of components
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws XmlPullParserException
	 */
	public List<ThirdPartyComponent> extractComponentsFromGradle(List<String> paths, String gradleFile)
			throws FileNotFoundException, IOException, XmlPullParserException {
		List<ThirdPartyComponent> components = new ArrayList<>();

		for (String path : paths) {
			components.addAll(extractComponentsFromGradle(path + gradleFile));
		}

		return components;
	}

	/**
	 * Extract components from a .gradle file
	 * <p>
	 * Convert each dependency found in a .gradle file into a list of components.
	 * 
	 * @param gradlePath the path to a build.gradle file
	 * @return a list of components
	 * @throws FileNotFoundException
	 */
	public List<ThirdPartyComponent> extractComponentsFromGradle(String gradleFilePath) throws FileNotFoundException {
		List<ThirdPartyComponent> components = new ArrayList<>();

		File gradleFile = new File(gradleFilePath);
		if (gradleFile.exists()) {
			LOG.info("Extracting components from gradle: " + gradleFile.getAbsolutePath());

			components.addAll(extractComponentsFromGradle(gradleFile));
		} else {
			LOG.info("Gradle file not found: " + gradleFile.getAbsolutePath());
		}

		return components;
	}

	/**
	 * Extract components from a .gradle file
	 * 
	 * @param gradleFile a gradle file containing components
	 * @return list of components
	 * @throws FileNotFoundException
	 */
	public List<ThirdPartyComponent> extractComponentsFromGradle(File gradleFile) throws FileNotFoundException {
		List<ThirdPartyComponent> components = new ArrayList<>();

		Scanner scanner = new Scanner(gradleFile);
		while (scanner.hasNextLine()) {
			String gradleDependency = scanner.nextLine().trim();

			for (DependencyToken token : GRADLE_DEPENDENCY_TOKENS) {
				ThirdPartyComponent component = componentFromGradleDependency(gradleDependency, token.getTokenBegin(),
						token.getTokenEnd());
				if (component != null) {
					components.add(component);
				}
			}
		}
		scanner.close();

		return components;
	}

	/**
	 * Extracts a component from a string obtained from a gradle file
	 * <p>
	 * It must test if the string contains a possible entry among those defined in
	 * GRADLE_DEPENDENCY_TOKENS
	 * 
	 * @param gradleDependency a string that is supposed to contain information
	 *                         about a dependency
	 * @param tokenBegin       the beginning a candidate markup
	 * @param tokenEnd         the end of a candidate markup
	 * @return a new third-party component obtained from the component
	 */
	private ThirdPartyComponent componentFromGradleDependency(String gradleDependency, String tokenBegin,
			String tokenEnd) {
		ThirdPartyComponent component = null;

		if (gradleDependency.startsWith(tokenBegin)) {
			gradleDependency = gradleDependency.substring(tokenBegin.length());
			gradleDependency = gradleDependency.substring(0, gradleDependency.length() - tokenEnd.length());

			StringTokenizer tokenizer = new StringTokenizer(gradleDependency, ":");
			String[] tks = new String[tokenizer.countTokens()];
			int count = 0;
			while (tokenizer.hasMoreTokens()) {
				tks[count++] = tokenizer.nextToken();

			}
			// import + name + version
			if (tks.length == 3) {
				component = new ThirdPartyComponent();

				component.setImport(tks[0]);
				component.setName(tks[1]);
			}
		}

		return component;
	}

	/**
	 * Extract components from a list of pom files
	 * <p>
	 * It tries to find a valid pom file stored in a list of paths prior to parsing
	 * it. This is a necessary step as some projects contain pom files in subfolders
	 * 
	 * @param paths      list of paths where pom files might be stored in
	 * @param gradleFile name of a pom file
	 * @return a list of components
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws XmlPullParserException
	 */
	public List<ThirdPartyComponent> extractComponentsFromPom(List<String> paths, String pomFile)
			throws FileNotFoundException, IOException, XmlPullParserException {
		List<ThirdPartyComponent> components = new ArrayList<>();

		for (String path : paths) {
			components.addAll(extractComponentsFromPom(path + pomFile));
		}

		return components;
	}

	/**
	 * Extract components from a POM file
	 * <p>
	 * Convert each dependency found in a pom.xml file into a list of components
	 * 
	 * @param pomFilePath to a pom file containing components
	 * @return a list of components
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws XmlPullParserException
	 */
	public List<ThirdPartyComponent> extractComponentsFromPom(String pomFilePath)
			throws FileNotFoundException, IOException, XmlPullParserException {
		List<ThirdPartyComponent> components = new ArrayList<>();

		File pomFile = new File(pomFilePath);
		if (pomFile.exists()) {
			LOG.info("Extracting components from pom: " + pomFile.getAbsolutePath());

			components.addAll(extractComponentsFromPom(pomFile));
		} else {
			LOG.info("Pom file not found: " + pomFile.getAbsolutePath());
		}

		return components;
	}

	/**
	 * Extract components from a POM file
	 * 
	 * @param pomFile a pom file containing components
	 * @return list of components
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws XmlPullParserException
	 */
	public List<ThirdPartyComponent> extractComponentsFromPom(File pomFile)
			throws FileNotFoundException, IOException, XmlPullParserException {
		MavenXpp3Reader mavenReader = new MavenXpp3Reader();
		Model model = mavenReader.read(new FileReader(pomFile));
		model.setPomFile(pomFile);

		List<ThirdPartyComponent> components = new ArrayList<>();
		components.addAll(fromPomDependenciesToComponents(model.getDependencies()));

		DependencyManagement dependencyManagement = model.getDependencyManagement();
		if (dependencyManagement != null) {
			components.addAll(fromPomDependenciesToComponents(dependencyManagement.getDependencies()));
		}

		return components;
	}

	/**
	 * Convert from a list of dependencies to a list of components
	 * 
	 * @param pomDependencies a list of dependencies stored in a pom file
	 * @return a list of components
	 */
	private List<ThirdPartyComponent> fromPomDependenciesToComponents(List<Dependency> pomDependencies) {
		List<ThirdPartyComponent> components = new ArrayList<>();

		for (Dependency pomDependency : pomDependencies) {
			ThirdPartyComponent component = new ThirdPartyComponent();

			component.setName(pomDependency.getArtifactId());
			component.setImport(pomDependency.getGroupId());

			components.add(component);
		}

		return components;
	}

	/**
	 * Add only new non-repeated components to components list
	 * <p>
	 * As the collection of components is the same for all mined references, we need
	 * to prevent adding duplicated ones
	 * 
	 * @param newComponents list of new components
	 * @return instance of this (ThirdPartyComponentsMiner)
	 */
	private ThirdPartyComponentsMiner appendNewComponents(List<ThirdPartyComponent> newComponents) {
		for (ThirdPartyComponent newComponent : newComponents) {
			if (newComponentNotInComponents(newComponent)) {
				thirdPartyComponents.add(newComponent);
			}
		}

		return this;
	}

	private boolean newComponentNotInComponents(ThirdPartyComponent newComponent) {
		boolean notIn = true;

		for (ThirdPartyComponent component : thirdPartyComponents) {
			if (newComponent.equals(component)) {
				notIn = false;

				break;
			}
		}

		return notIn;
	}

	/**
	 * save all components' categories to database
	 * 
	 * @return instance of this (ThirdPartyComponentsMiner)
	 */
	private ThirdPartyComponentsMiner saveCategories() {
		AksDAO dao = AksDAO.newCategoryDAO();

		List<Document> components = fromComponentsToDocs(groupComponentsByCategory(thirdPartyComponents));
		for (Document component : components) {
			dao.insert(component);
		}

		return this;
	}

	/**
	 * save all concerns to database
	 * 
	 * @return instance of this (ThirdPartyComponentsMiner)
	 */
	private ThirdPartyComponentsMiner saveConcerns() {
		AksDAO dao = AksDAO.newConcernDAO();

		List<Document> concerns = fromComponentsToDocs(groupComponentsByConcern(thirdPartyComponents));
		for (Document concern : concerns) {
			dao.insert(concern);
		}

		return this;

	}

	/**
	 * Remove an undesired components
	 * <p>
	 * Our criteria to consider a component undesired:
	 * <p>
	 * Some components ids are not parseable and/or they represent a group of many
	 * dependencies in the original POM/Gradle file. We will try to include these
	 * types of dependencies in the future.
	 * 
	 * @return instance of this (ThirdPartyComponentsMiner)
	 * @throws IOException
	 */
	private ThirdPartyComponentsMiner excludeUndesiredComponents() throws IOException {
		List<ThirdPartyComponent> components = new ArrayList<>();

		List<String> excludeds = componentsConfig.getExcluded();
		for (ThirdPartyComponent externalDependency : thirdPartyComponents) {
			if (!excludeds.contains(externalDependency.getKey())) {
				components.add(externalDependency);
			} else {
				LOG.info("Dependency " + externalDependency + " was removed from analysis!");
			}
		}

		thirdPartyComponents.clear();
		thirdPartyComponents.addAll(components);

		return this;
	}

	/**
	 * Fill in the category of each component
	 * 
	 * @return instance of this (ThirdPartyComponentsMiner)
	 * @throws IOException
	 */
	private ThirdPartyComponentsMiner fillCategory() throws IOException {
		Map<String, String> localCategories = componentsConfig.getCategories();

		for (ThirdPartyComponent component : thirdPartyComponents) {
			LOG.info("Mining category for: " + component);

			// try to find category in the local dataset...
			if (localCategories.containsKey(component.getKey())) {
				component.setCategory(localCategories.get(component.getKey()));

				LOG.info("Category found: " + component.getCategory());
			} else {
				// ... to avoid mining it from MVNRepository
				mavenMiner.configure(component).mine();
				if (component.getCategory().equals(MavenMiner.UNCATEGORIZED_COMPONENT)) {
					Set<String> tags = component.getTags();

					component.setCategory((tags == null) || (tags.isEmpty()) ? MavenMiner.UNCATEGORIZED_COMPONENT
							: "Tags" + component.getTags());
				}
				LOG.info("Category mined from MVNRepository: " + component.getCategory());
			}
		}

		return this;
	}

	/**
	 * Fill in the concern of each component
	 * 
	 * @return instance of this (ThirdPartyComponentsMiner)
	 * @throws IOException
	 */
	private ThirdPartyComponentsMiner fillConcern() throws IOException {
		Map<String, Set<String>> localConcerns = componentsConfig.getConcerns();

		for (ThirdPartyComponent component : thirdPartyComponents) {
			LOG.info("Mining concern for: " + component.getKey());

			component.setConcerns(null);
			component.setMatch(null);
			String impoRt = component.getImport();

			// try to locate the exact concern in the local dataset
			if (localConcerns.containsKey(impoRt)) {
				component.setConcerns(localConcerns.get(impoRt));
				// confirms that the entire import matches
				component.setMatch(impoRt);
			} else {
				// try a partial match
				for (String id : localConcerns.keySet()) {
					if (impoRt.startsWith(id)) {
						component.setConcerns(localConcerns.get(id));
						// the import becomes the partial matched import
						component.setMatch(id);

						break;
					}
				}
			}

			if (component.getConcerns() == null) {
				LOG.error("No concerns/Matches found for " + component);
			} else {
				LOG.info("Concerns/Matches found: " + component.getConcerns() + "/" + component.getMatch());
			}
		}

		return this;
	}

	/**
	 * Group the list of components according to their categories
	 * 
	 * @param components list of components to be grouped
	 * @return a mapping of components per category
	 */
	private Map<String, List<ThirdPartyComponent>> groupComponentsByCategory(List<ThirdPartyComponent> components) {
		Map<String, List<ThirdPartyComponent>> componentsByCategory = new HashMap<>();

		for (ThirdPartyComponent component : components) {
			String category = component.getCategory();

			List<ThirdPartyComponent> groupedComponents = new ArrayList<>();
			if (componentsByCategory.containsKey(category)) {
				groupedComponents = componentsByCategory.get(category);
			} else {
				componentsByCategory.put(category, groupedComponents);
			}

			groupedComponents.add(component);
		}

		return componentsByCategory;
	}

	/**
	 * Group the list of components according to their concerns
	 * 
	 * @param components list of components to be grouped
	 * @return a mapping of components per concern
	 */
	private Map<String, List<ThirdPartyComponent>> groupComponentsByConcern(List<ThirdPartyComponent> components) {
		Map<String, List<ThirdPartyComponent>> componentsByConcern = new HashMap<>();

		for (ThirdPartyComponent component : components) {
			Set<String> concerns = component.getConcerns();
			if (concerns == null) {
				LOG.error("No concerns for component: " + component.getKey());
			} else {
				for (String concern : concerns) {
					List<ThirdPartyComponent> groupedComponents = new ArrayList<>();
					if (componentsByConcern.containsKey(concern)) {
						groupedComponents = componentsByConcern.get(concern);
					} else {
						componentsByConcern.put(concern, groupedComponents);
					}

					groupedComponents.add(component);
				}
			}
		}

		return componentsByConcern;
	}

	/**
	 * Convert the list of dependencies to a list do bson documents
	 * <p>
	 * Prior to saving the list of components, it must be converted to a list of
	 * documents. In addition, each document is mapped to the code analysis
	 * (obtained from the rm_code_analysis collection).
	 * 
	 * @param groupedComponents list of categorized components
	 * @return list of documents, each one representing a component
	 */
	private List<Document> fromComponentsToDocs(Map<String, List<ThirdPartyComponent>> groupedComponents) {
		List<Document> docs = new ArrayList<>();

		CodeAnalysisDAO dao = new CodeAnalysisDAO();
		List<Document> analysis = dao.findMany(Filters.gt("filehash", 0), // (useless) filter needed to fetch all
				Projections.include("_id", "imports", "reference"), null);

		// for each category in the map of grouped components...
		for (String key : groupedComponents.keySet()) {
			Document componentDoc = new Document();
			// ...create/append a new component document...
			componentDoc.append("repository", metricsMiner.getRepositoryId()).append("dimension", this.dimension)
					.append("name", key);

			List<Document> componentz = new ArrayList<>();
			Map<String, Set<ObjectId>> affectedAnalysis = new HashMap<>();
			// ...and for each component under the category...
			List<ThirdPartyComponent> components = groupedComponents.get(key);
			for (ThirdPartyComponent component : components) {
				// ...create a new document to associate the component with code analysis...
				Document doc = new Document();
				doc.append("name", component.getName()).append("import", component.getImport()).append("match",
						component.getMatch());
				componentz.add(doc);
				// ...and fill in the list of code analysis that are associated the
				// component
				LOG.info("Retrieving analysis affected by " + component);
				fillAffectedAnalysis(affectedAnalysis, analysis, component.getMatch());
			}
			// append the list of components to the new component doc...
			componentDoc.append("components", componentz);

			// ...link the component to each affected code analysis...
			List<Document> references = new ArrayList<>();
			for (String reference : affectedAnalysis.keySet()) {
				Document referenceDoc = new Document();
				referenceDoc.append("reference", reference);
				referenceDoc.append("code_analysis",
						mineImportedComponents(components, affectedAnalysis.get(reference)));

				references.add(referenceDoc);
			}

			// ...and save the list of links
			componentDoc.append("references", references);
			docs.add(componentDoc);
		}

		return docs;
	}

	public List<Document> mineImportedComponents(List<ThirdPartyComponent> components, Set<ObjectId> analysisIds) {
		List<Document> docs = new ArrayList<Document>();

		ImportedComponentsMiner miner = new ImportedComponentsMiner();
		for (ObjectId id : analysisIds) {
			docs.add(miner.configure(components, id).mine());
		}

		return docs;
	}

	/**
	 * Consult the collection of code analysis to determine which ones matches the
	 * imported the component
	 * 
	 * @param affectedAnalysis the ids of code analysis that contains files that
	 *                         import the component
	 * @param match
	 */
	@SuppressWarnings("unchecked")
	private void fillAffectedAnalysis(Map<String, Set<ObjectId>> affectedAnalysis, List<Document> analysis,
			String match) {
		if ((match == null) || (match.isEmpty())) {
			LOG.error("No match for the imported component. Unable to fill affected analysis!");
		} else {
			for (Document a : analysis) {
				String reference = a.getString("reference");

				List<Document> imports = (List<Document>) a.get("imports");
				for (Document i : imports) {
					if (i.getString("name").contains(match)) {
						Set<ObjectId> ids = new HashSet<>();
						if (affectedAnalysis.containsKey(reference)) {
							ids = affectedAnalysis.get(reference);
						} else {
							affectedAnalysis.put(reference, ids);
						}
						ids.add(a.getObjectId("_id"));
					}
				}
			}
		}
	}

	@Override
	public String getName() {
		return "Third-Party Components Miner";
	}

}
