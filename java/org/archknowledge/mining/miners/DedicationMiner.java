package org.archknowledge.mining.miners;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.archknowledge.mining.Miner;
import org.archknowledge.model.ComponentsConfig;
import org.archknowledge.persistence.AksDAO;
import org.archknowledge.persistence.StaticDAO;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.repositoryminer.metrics.RepositoryMinerMetrics;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.googlejavaformat.java.Formatter;
import com.google.googlejavaformat.java.FormatterException;

/**
 * Measure the degree to which source code artifacts are dedicated to concerns
 * <p>
 * Measurement strategy is based on the number of methods that refers to the
 * types of classes that implement a given concern. It includes methods that:
 * (i) instantiates/returns instances or uses attributes whose types are
 * injected from such classes; or (ii) are modified by annotations whose types
 * implement the concern.
 * 
 * @author Luis Paulo
 */
public class DedicationMiner implements Miner {

	protected static final Logger LOG = LoggerFactory.getLogger(DedicationMiner.class);

	private RepositoryMinerMetrics metricsMiner;
	private boolean inspecting;
	private String dimension;

	private List<String> references;

	public DedicationMiner() {
		references = new ArrayList<>();
	}

	@Override
	public Miner configure(RepositoryMinerMetrics metricsMiner) {
		this.metricsMiner = metricsMiner;

		return this;
	}

	@Override
	public Miner configure(ComponentsConfig componentsConfig) {
		return this;
	}

	@Override
	public Miner configure(String dimension) {
		this.dimension = dimension;

		return this;
	}

	@Override
	public Miner configure(boolean inspecting) {
		this.inspecting = inspecting;

		return this;
	}

	@Override
	public Miner execute(String reference) throws Exception {
		references.add(reference);

		return this;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Miner postExecute() throws Exception {
		if (!inspecting) {
			List<Document> concerns = AksDAO.newConcernDAO().findMany(null, null);

			AksDAO dao = AksDAO.newDedicationDAO();
			// for each concern...
			for (Document concern : concerns) {
				String concernName = concern.getString("name");

				List<Document> references = (List<Document>) concern.get("references");
				// and for each reference affecting the concern...
				for (Document reference : references) {
					List<Document> analysises = (List<Document>) reference.get("code_analysis");
					// ...let's calculate the complexity associate to each code analysis...
					for (Document analysis : analysises) {
						ObjectId analysisId = analysis.getObjectId("id");
						Document codeAnalysis = StaticDAO.getCodeAnalysis(analysisId);

						List<String> imports = (List<String>) analysis.get("imports");

						// ...by measuring the dedication by methods
						List<Document> classes = measure(concernName, imports, codeAnalysis);
						analysis.append("source", annotate(concernName, codeAnalysis, classes, imports));
						analysis.append("NOC", classes.size());
						analysis.append("classes", removePositions(classes));

						Document doc = new Document();
						doc.append("repository", metricsMiner.getRepositoryId()).append("dimension", this.dimension)
								.append("concern", concernName).append("components", concern.get("components"))
								.append("reference", reference.getString("reference"))
								.append("code_analysis", analysis);

						dao.insert(doc);
					}
				}
			}
		}

		return this;
	}

	@SuppressWarnings("unchecked")
	private List<Document> removePositions(List<Document> dedications) {
		for (Document dedication : dedications) {
			List<Document> methods = (List<Document>) dedication.get("methods");

			for (Document method : methods) {
				method.remove("position");
			}
		}

		return dedications;
	}

	private String annotate(String concern, Document codeAnalysis, List<Document> dedications,
			List<String> importedComponents) {
		String fileName = codeAnalysis.getString("filename");
		String source = codeAnalysis.getString("source");

		LOG.info("Annotating concern '" + concern + "' in file: " + fileName);

		try {
			source = annotateMethods(concern, dedications, source);
			source = annotateImports(concern, importedComponents, source);
		} catch (FormatterException e) {
			source = "";

			LOG.error("Unable to annotate concern " + concern + " in file: " + fileName);
		}

		return source;
	}

	private String annotateImports(String concern, List<String> importedComponents, String source)
			throws FormatterException {
		String annotated = new Formatter().formatSource(source);

		String annotation = "@Concern(name=\"" + concern + "\")\n";

		for (String component : importedComponents) {
			String impoRt = "import " + component + ";";
			String impoRtStatic = "import static " + component + ";";

			annotated = annotated.replaceAll(impoRt, annotation + impoRt).replaceAll(impoRtStatic,
					annotation + impoRtStatic);
		}

		return annotated;
	}

	@SuppressWarnings("unchecked")
	private String annotateMethods(String concern, List<Document> dedications, String source) {
		String annotated = "";

		List<Integer> positions = new ArrayList<>();

		for (Document doc : dedications) {
			List<Document> methods = (List<Document>) doc.get("methods");
			for (Document method : methods) {
				int NOR = method.getInteger("NOR", 0);
				if (NOR > 0) {
					positions.add(method.getInteger("position"));
				}
			}
		}

		int pos = 0;
		while (pos < source.length()) {
			if (positions.contains(pos)) {
				annotated += "@Concern(name = \"" + concern + "\")\n";
			}
			annotated += source.charAt(pos);

			pos++;
		}

		return annotated;
	}

	/**
	 * Calculate dedication by method
	 * <p>
	 * For all classes associated with an analysis check if the components that
	 * implement a concern are referenced by classes' methods
	 * 
	 * @param concern      the concern being analyzed
	 * @param imports      list of imports used to implement the concern
	 * @param codeAnalysis the analysis doc associated with a file and its classes
	 * 
	 * @return list of documents containing all analyzed classes
	 */
	@SuppressWarnings("unchecked")
	public List<Document> measure(String concern, List<String> imports, Document codeAnalysis) {
		List<Document> dedications = new ArrayList<>();

		// for each class found in the artifact...
		List<Document> classes = (List<Document>) codeAnalysis.get("classes");
		for (Document clazz : classes) {

			Map<String, Document> dedicationByMethod = new HashMap<>();
			List<Document> fields = (List<Document>) clazz.get("fields");
			// ...lets check if each component...
			for (String impoRt : imports) {
				String className = impoRt.substring(impoRt.lastIndexOf(".") + 1);

				// ...is being accessed by the class' methods
				List<Document> methods = (List<Document>) clazz.get("methods");
				for (Document method : methods) {
					String methodName = method.getString("name");

					Document dedication = new Document();
					if (dedicationByMethod.containsKey(methodName)) {
						dedication = dedicationByMethod.get(methodName);
					} else {
						dedication.append("source", method.getString("source"))
								.append("is_empty", methodIsEmpty(method)).append("NOR", 0)
								.append("position", method.getInteger("begins_at"));
					}

					if (isReferencedByMethod(className, fields, method)) {
						Integer NOR = dedication.getInteger("NOR", 0) + 1;
						dedication.replace("NOR", NOR);
					}

					dedicationByMethod.put(methodName, dedication);
				}
			}

			dedications.add(new Document().append("class", clazz.getString("name"))
					.append("is_interface", clazz.getBoolean("is_interface"))
					.append("source", clazz.getString("source")).append("methods", toDocuments(dedicationByMethod)));
		}

		return dedications;
	}

	@SuppressWarnings("unchecked")
	private boolean methodIsEmpty(Document method) {
		List<Document> statements = (List<Document>) method.get("statements");

		return (statements.size() == 0);
	}

	/**
	 * Check if a component is being accessed
	 * 
	 * @param className names a component used to implement a concern
	 * @param fields    list of fields in the class
	 * @param method    the method being analyzed
	 * 
	 * @return true if (i) an annotation attached to the method whose type is that
	 *         of the component; (ii) the class of the component is directly
	 *         referenced somewhere in method's source code; (iii) a field whose
	 *         type is that of the component is referenced by the method
	 */
	@SuppressWarnings("unchecked")
	private boolean isReferencedByMethod(String className, List<Document> fields, Document method) {
		boolean is = false;

		List<String> modifiers = (List<String>) method.get("modifiers");
		if (componentIsReferenced(className, modifiers)) { // (i)
			is = true;
		} else {
			String source = method.getString("source");
			if ((source != null)) {
				source = source.replace("\n", "");
				if (componentIsReferenced(className, source) // (ii)
						|| (componentIsReferenced(className, fields, source))) { // (iii)
					is = true;
				}
			}
		}

		return is;
	}

	private boolean componentIsReferenced(String className, List<String> modifiers) {
		boolean is = false;

		for (String modifier : modifiers) {
			if (("@" + className).equals(modifier)) {
				is = true;

				break;
			}
		}

		return is;
	}

	private List<Document> toDocuments(Map<String, Document> dedicationByMethod) {
		List<Document> docs = new ArrayList<>();

		for (Map.Entry<String, Document> entry : dedicationByMethod.entrySet()) {
			Document doc = entry.getValue();

			docs.add(new Document().append("method", entry.getKey()).append("source", doc.getString("source"))
					.append("is_empty", doc.getBoolean("is_empty")).append("NOR", doc.getInteger("NOR"))
					.append("position", doc.getInteger("position")));
		}

		return docs;
	}

	private boolean componentIsReferenced(String className, List<Document> fields, String source) {
		boolean is = false;

		List<String> names = getFieldNames(className, fields);
		for (String name : names) {
			if (is = source.matches(".*?\\b" + name + "\\b.*?")) {
				break;
			}
		}

		return is;
	}

	@SuppressWarnings("unchecked")
	public List<String> getFieldNames(String className, List<Document> fields) {
		List<String> names = new ArrayList<>();

		// two ways to select fields:
		for (Document field : fields) {
			String fieldName = field.getString("name");
			// (i) if the type of the field is that of class name
			if (field.getString("type").endsWith(className)) {
				names.add(fieldName);
			} else {
				// (ii) at least one modifier is an annotation whose type is that of class name
				List<String> modifiers = (List<String>) field.get("modifiers");
				for (String modifier : modifiers) {
					if (("@" + className).equals(modifier)) {
						names.add(fieldName);

						break;
					}
				}

			}
		}

		return names;
	}

	private boolean componentIsReferenced(String className, String source) {
		boolean is = source.matches(".*?\\b" + className + "\\b.*?");

		return is;
	}

	@Override
	public String getName() {
		return "Dedication Miner";
	}

}
