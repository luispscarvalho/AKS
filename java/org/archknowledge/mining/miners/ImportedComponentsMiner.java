package org.archknowledge.mining.miners;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.archknowledge.model.ThirdPartyComponent;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.repositoryminer.metrics.persistence.CodeAnalysisDAO;

import com.mongodb.client.model.Projections;

/**
 * Mines the number of imports (NOI) and the number of imported components (NOIC)
 * from source code artifacts
 * <p>
 * The mining routines are called by {@link ThirdPartyComponentsMiner} in order
 * to find out which classes/resources are being imported from a component that
 * is used to implement a concern. As a result, a bson document is generated to
 * inform: (i) the name of the file from which the imports were extracted; (ii)
 * the total Number Of Imports (NOI); and the Number Of Imported Components
 * (NOIC).
 * <p>
 * From NOI we exclude native JDK's imports. Usually, the java.* will be used to
 * instantiate data structures or basic programming utilities which have generic
 * purpose, not focusing on the implementation of specific concerns.
 * <p>
 * The values of NOI and NOIC are used to calculate the value of Imported
 * Components Dedication (ICD).
 * 
 * @author Luis Paulo
 */
public class ImportedComponentsMiner {

	private List<ThirdPartyComponent> components;
	private ObjectId analysisId;

	public ImportedComponentsMiner configure(List<ThirdPartyComponent> components, ObjectId analysisId) {
		this.components = components;
		this.analysisId = analysisId;

		return this;
	}

	/**
	 * Mines information about the imported components
	 * <p>
	 * The list of imports of each artifact is analyzed to obtain a list of those
	 * that are associated with the implementation of a concern.
	 * <p>
	 * The values of NOI, NOIC and ICo
	 * 
	 * @return a json document encapsulating all imported components data
	 */
	@SuppressWarnings("unchecked")
	public Document mine() {
		// lets create a new document to append all necessary information
		Document doc = new Document().append("id", analysisId);
		// NOI is calculated from the number of imports after the undesired one are
		// excluded
		Document analysis = getAnalysisDoc();
		List<Document> imports = excludeUndesired((List<Document>) analysis.get("imports"));

		doc.append("filename", analysis.getString("filename"));
		doc.append("NOI", imports.size());
		// NOID is calculated by analyzing which imports used to implement the concern
		// is found in the list of artifacts' imports
		Set<String> referencedImports = new HashSet<>();
		Set<String> uniqueComponents = getUniqueMatches();
		for (String match : uniqueComponents) {
			for (Document impoRt : imports) {
				String name = impoRt.getString("name");
				if (name.contains(match + ".")) {
					referencedImports.add(name);
				}
			}
		}
		doc.append("NOIC", referencedImports.size());
		doc.append("ICD", calculateICD(imports.size(), referencedImports.size()));
		doc.append("imports", referencedImports);

		return doc;
	}

	/**
	 * Calculates the Imported Components Dedication (ICD) from the number of
	 * imported components (NOIC) and the total number of imports (NOI).
	 * 
	 * @param Number Of Imports (NOI)
	 * @param Number Of Imported Components (NOIC)
	 * @return value of ICD
	 */
	public double calculateICD(int NOI, int NOIC) {
		double ICD = NOIC * 1.0 / NOI;
		ICD = (Math.floor(ICD * 100)) / 100;

		return ICD;
	}

	private Set<String> getUniqueMatches() {
		Set<String> uniqueMatches = new HashSet<String>();

		for (ThirdPartyComponent component : components) {
			uniqueMatches.add(component.getMatch());
		}

		return uniqueMatches;
	}

	/**
	 * As aforementioned native imports are excluded since they are not dedicated to
	 * implement high level concerns
	 * 
	 * @param imports list of imports found in the artifacts
	 * @return new list of imports from which native imports were removed
	 */
	private List<Document> excludeUndesired(List<Document> imports) {
		List<Document> newImports = new ArrayList<>();

		for (Document importt : imports) {
			if (!importt.getString("name").strip().startsWith("java.")) {
				newImports.add(importt);
			}
		}

		return newImports;
	}

	private Document getAnalysisDoc() {
		CodeAnalysisDAO dao = new CodeAnalysisDAO();

		return dao.findById(analysisId, Projections.include("filename", "source", "imports"));
	}

}
