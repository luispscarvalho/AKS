package org.archknowledge.mining;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import org.archknowledge.mining.miners.ThirdPartyComponentsMiner;
import org.archknowledge.model.ComponentsConfig;
import org.archknowledge.model.ThirdPartyComponent;
import org.archknowledge.persistence.AksDAO;
import org.archknowledge.persistence.DAO;
import org.bson.Document;
import org.repositoryminer.RepositoryMiner;
import org.repositoryminer.domain.Repository;
import org.repositoryminer.domain.SCMType;
import org.repositoryminer.metrics.MetricsConfig;
import org.repositoryminer.metrics.RepositoryMinerMetrics;
import org.repositoryminer.metrics.codemetric.AMW;
import org.repositoryminer.metrics.codemetric.ATFD;
import org.repositoryminer.metrics.codemetric.CYCLO;
import org.repositoryminer.metrics.codemetric.CodeMetric;
import org.repositoryminer.metrics.codemetric.FDP;
import org.repositoryminer.metrics.codemetric.LAA;
import org.repositoryminer.metrics.codemetric.LOC;
import org.repositoryminer.metrics.codemetric.LVAR;
import org.repositoryminer.metrics.codemetric.MAXNESTING;
import org.repositoryminer.metrics.codemetric.NOA;
import org.repositoryminer.metrics.codemetric.NOAM;
import org.repositoryminer.metrics.codemetric.NOAV;
import org.repositoryminer.metrics.codemetric.NOM;
import org.repositoryminer.metrics.codemetric.NOPA;
import org.repositoryminer.metrics.codemetric.NProtM;
import org.repositoryminer.metrics.codemetric.PAR;
import org.repositoryminer.metrics.codemetric.TCC;
import org.repositoryminer.metrics.codemetric.WMC;
import org.repositoryminer.metrics.codemetric.WOC;
import org.repositoryminer.metrics.codesmell.BrainClass;
import org.repositoryminer.metrics.codesmell.BrainMethod;
import org.repositoryminer.metrics.codesmell.CodeSmell;
import org.repositoryminer.metrics.codesmell.ComplexMethod;
import org.repositoryminer.metrics.codesmell.DataClass;
import org.repositoryminer.metrics.codesmell.FeatureEnvy;
import org.repositoryminer.metrics.codesmell.GodClass;
import org.repositoryminer.metrics.codesmell.LongMethod;
import org.repositoryminer.metrics.parser.Parser;
import org.repositoryminer.metrics.parser.java.JavaParser;
import org.repositoryminer.metrics.persistence.CodeAnalysisReportDAO;
import org.repositoryminer.persistence.RepositoryDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Projections;

/**
 * Main class for mining software projects
 * <p>
 * This class is responsible for: mining metrics, detecting smells, extracting
 * components from pom/gradle files and categorizing them.
 * <p>
 * Mining of metrics and detection of smells is performed by Repository Miner
 * (RM). Thus, RM's mining routines must be called first.
 * <p>
 * During the execution of RM, miners from the miners list are executed to
 * perform specialized mining routines. All miners must comply with the methods
 * defined in {@link Miner}.
 * <p>
 * Configuration method (e.g., {@link #configure(ComponentsConfig)},
 * {@link #configureMetrics()}) must be called first in order to prepare the
 * miner for executing the miners via the {@link #mine()} method.
 * 
 * @author Luis Paulo
 */
public class ArchKnowledgeMiner {

	private static final Logger LOG = LoggerFactory.getLogger(ArchKnowledgeMiner.class);

	private RepositoryMiner repoMiner;
	private MetricsConfig metricsConfig;

	private ComponentsConfig componentsConfig;

	private List<Miner> miners;

	private String dimension, mainBranch;
	private Map<String, String> frames;

	private Set<ThirdPartyComponent> uncategorizedComponents;

	public ArchKnowledgeMiner() {
		repoMiner = new RepositoryMiner();

		miners = new ArrayList<>();
		uncategorizedComponents = new HashSet<>();
	}

	public ArchKnowledgeMiner configure(String dimension) {
		this.dimension = dimension;

		return this;
	}

	public ArchKnowledgeMiner configure(Document config) {
		String name = config.getString("name").toLowerCase();

		LOG.info("Configuring miner for project: " + name);

		repoMiner.setKey(name);
		repoMiner.setName(name);
		repoMiner.setDescription(config.getString("description") + " (" + config.getString("url") + ")");
		repoMiner.setPath(config.getString("path"));
		repoMiner.setSCM(SCMType.GIT);

		mainBranch = config.getString("main_branch");
		metricsConfig = configureMetrics();

		loadFrames(config);

		return this;
	}

	public ArchKnowledgeMiner configure(ComponentsConfig componentsConfig) {
		this.componentsConfig = componentsConfig;
		uncategorizedComponents.clear();

		return this;
	}

	public ArchKnowledgeMiner configure(List<Miner> miners) {
		// mining of components comes first...
		this.miners.add(new ThirdPartyComponentsMiner().configure(new UncategorizedComponentListener() {
			@Override
			public UncategorizedComponentListener onUncategorizedComponent(ThirdPartyComponent component) {
				uncategorizedComponents.add(component);

				return this;
			}
		}));
		// ...so that the next miners will have concerns to mine specific data from
		this.miners.addAll(miners);

		return this;
	}

	/**
	 * Configure all metrics and smells as provided by RM
	 * 
	 * @return instance of {@link MetricsConfig}
	 */
	private MetricsConfig configureMetrics() {
		MetricsConfig config = new MetricsConfig();

		List<Parser> parsers = Arrays.asList(new JavaParser());
		List<CodeMetric> metrics = Arrays.asList(new LOC(), new CYCLO(), new AMW(), new ATFD(), new FDP(), new LAA(),
				new LVAR(), new MAXNESTING(), new NOA(), new NOAM(), new NOAV(), new NOM(), new NOPA(), new NProtM(),
				new PAR(), new TCC(), new WMC(), new WOC());
		List<CodeSmell> smells = Arrays.asList(new GodClass(), new LongMethod(), new DataClass(), new FeatureEnvy(),
				new BrainClass(), new BrainMethod(), new ComplexMethod());
		config.setParsers(parsers);
		config.setCodeMetrics(metrics);
		config.setCodeSmells(smells);

		return config;
	}

	/**
	 * Executes RM's and Miners' mining routines.
	 * 
	 * <p>
	 * Two mining steps:
	 * <ul>
	 * <li>Inspect - will only checkout references/snapshots to allow inspecting
	 * POM/Gradle files</li>
	 * <li>Mine - perform the actual mining</li>
	 * </ul>
	 * <p>
	 * 
	 * @param inspect if true indicates the first execution to capture components
	 * @return instance of this (ArchKnowlegdeMiner)
	 * @throws Exception
	 */
	public ArchKnowledgeMiner mine(boolean inspect) throws Exception {
		DAO.configure(dimension + "_" + repoMiner.getKey());

		Repository repository = null;
		if ((repository = findRepository(repoMiner.getKey())) == null) {
			repository = repoMiner.mine();
		}
		RepositoryMinerMetrics metricsMiner = newMetricsMiner(repository);

		// prepare miners for execution
		configureMiners(metricsMiner, inspect);

		LOG.info("Executing miners now...");

		// for each reference...
		for (String reference : frames.keySet()) {
			// ... inspect or mine...
			metricsMiner.run(mainBranch, reference, metricsConfig, inspect);
			// ... and execute all miners
			executeMiners(reference);
		}
		// anything left in miners to execute?
		postExecuteMiners();

		// save frames' refs and labels
		if (!inspect) {
			saveFrames();
		}

		metricsMiner.finish();

		return this;
	}

	private Repository findRepository(String key) {
		Repository repository = null;
		// new repo and miner
		RepositoryDAO repoDao = new RepositoryDAO();
		Document doc = repoDao.findByKey(repoMiner.getKey(), null);
		if (doc != null) {
			repository = Repository.parseDocument(doc);
		}

		return repository;
	}

	/**
	 * load config information about data frames
	 * <p>
	 * A data frame is a portion of the dataset that refers to a RM's reference and
	 * has a ordering label
	 * 
	 * @param config
	 * @return instance of this (ArchKnowlegdeMiner)
	 */
	@SuppressWarnings("unchecked")
	private ArchKnowledgeMiner loadFrames(Document config) {
		this.frames = new HashMap<>();

		List<Document> frames = (List<Document>) config.get("frames");
		for (Document frame : frames) {
			this.frames.put(frame.getString("reference"), frame.getString("label"));
		}

		return this;
	}

	/**
	 * save information about each data frame
	 * 
	 * @return instance of this (ArchKnowlegdeMiner)
	 */
	private ArchKnowledgeMiner saveFrames() {
		Map<Date, Document> orderedFrames = new TreeMap<>();
		for (Entry<String, String> frame : this.frames.entrySet()) {
			Date date = getReferenceDate(frame.getKey());

			Document doc = new Document();
			doc.append("reference", frame.getKey()).append("label", frame.getValue()).append("date", date);

			orderedFrames.put(date, doc);
		}

		AksDAO dao = AksDAO.newFrameDAO();
		dao.insertMany(new ArrayList<Document>(orderedFrames.values()));

		return this;
	}

	public Date getReferenceDate(String reference) {
		CodeAnalysisReportDAO dao = new CodeAnalysisReportDAO();
		Document report = dao.findOne(Filters.eq("reference", reference), Projections.include("commit_date"));

		return report.getDate("commit_date");
	}

	/**
	 * Configure all miners
	 * 
	 * @param metricsMiner RM's metrics miner
	 * @param inspecting   wars the miner whether a inspection is being carried out
	 *                     *
	 * @return instance of this (ArchKnowlegdeMiner)
	 */
	private ArchKnowledgeMiner configureMiners(RepositoryMinerMetrics metricsMiner, boolean inspecting) {
		for (Miner miner : miners) {
			miner.configure(metricsMiner).configure(componentsConfig).configure(dimension).configure(inspecting);
		}

		return this;
	}

	/**
	 * Run all miners
	 * 
	 * @param warns     the miners if a inspection is being carried out
	 * @param reference the reference (tag/branch/version) being mined
	 * 
	 * @return instance of this (ArchKnowlegdeMiner)
	 * @throws Exception
	 */
	private ArchKnowledgeMiner executeMiners(String reference) throws Exception {
		for (Miner miner : miners) {
			LOG.info("Executing " + miner.getName() + " now...");

			miner.execute(reference);
		}

		return this;
	}

	/**
	 * Run postExecute routines of each miner
	 * 
	 * @return instance of this (ArchKnowlegdeMiner)
	 * @throws Exception
	 */
	private ArchKnowledgeMiner postExecuteMiners() throws Exception {
		for (Miner miner : miners) {
			LOG.info("Post-executing " + miner.getName() + " now...");

			miner.postExecute();
		}

		return this;
	}

	/**
	 * create and configure a new metrics/smells miner
	 * 
	 * @param repository the repository being mined
	 * @return instance of this (ArchKnowlegdeMiner)
	 * @throws IOException
	 */
	private RepositoryMinerMetrics newMetricsMiner(Repository repository) throws IOException {
		RepositoryMinerMetrics miner = new RepositoryMinerMetrics();
		miner.init(repository.getKey());

		return miner;
	}

	public Set<ThirdPartyComponent> getUncategorizedComponents() {
		return uncategorizedComponents;
	}

}
