package org.archknowledge.mining;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashSet;
import java.util.stream.Collectors;

import org.archknowledge.config.Configurations;
import org.archknowledge.model.ThirdPartyComponent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Mine extra information about third-party components from MVNRepository
 * (https://mvnrepository.com)
 * <p>
 * This miner tries to extract components' categories from the MVNRepository web
 * site. MVNRepository contains information about categories of components,
 * which can be used to classify/group concerns.
 * <p>
 * The miner scans the html returned by the web site and parses metadata from
 * it. Ideally, MVNRepository should provide a REST(?) web service interface to
 * externalize the metadata as parsing raw html is inefficient and error-prone.
 * <p>
 * MVNRepository may block your IP number if this miner accesses MVNRepository
 * too many times within a too short interval of time. To avoid being blocked,
 * we restrained the mining to not reach MVNRepository too soon after each
 * contact. The interval can be defined by setting the value of
 * MAVEN_REPOSITORY_ACCESS_INTERVAL in {@link Configurations}.
 * <p>
 * Not all components have been categorized by MVNRepository. This means: this
 * miner may not provide a category for components sometimes. As a consequence,
 * the missing components' categories (and concerns) must be filled manually.
 * This can be done by injecting an instance of
 * {@link UncategorizedComponentListener} in the
 * {@link #configure(UncategorizedComponentListener)} method.
 * 
 * @author Luis Paulo
 */
public class MavenMiner {

	private static final Logger LOG = LoggerFactory.getLogger(MavenMiner.class);

	public static final String UNCATEGORIZED_COMPONENT = "uncategorized";

	private static final String METADATA_MARKUPS = "<table class=\"grid\" width=\"100%\"><tr><th>License</th><td>";
	private static final String[] CATEGORY_MARKUPS = new String[] { "<th>Categories</th><td><a href=\"/open-source/",
			" class=\"b c\">", "</a></td></tr>" };
	private static final String[] TAGS_MARKUPS = new String[] { "<td><a href=\"/tags", " class=\"b tag\"", "</a>" };

	private UncategorizedComponentListener listener;
	private ThirdPartyComponent component;

	/**
	 * Set an instance of {@link UncategorizedComponentListener}
	 * <p>
	 * The listener can be used by callers to keep track of all uncategorized
	 * components.
	 * 
	 * @param listener
	 * @return instance of this (MavenMiner)
	 */
	public MavenMiner configure(UncategorizedComponentListener listener) {
		this.listener = listener;

		return this;
	}

	/**
	 * Set the component whose category must be filled in
	 * 
	 * @param component whose category is missing
	 * @return instance of MavenMiner (this)
	 */
	public MavenMiner configure(ThirdPartyComponent component) {
		this.component = component;

		return this;
	}

	/**
	 * Activate the miner
	 * <p>
	 * It navigates to "https://mvnrepository.com/artifact/" and parses the
	 * response/html.
	 * 
	 * @return instance of MavenMiner (this)
	 */
	public MavenMiner mine() {
		component.setCategory(UNCATEGORIZED_COMPONENT);

		try {
			String html = getRawHTML();
			fillCategory(html).fillTags(html);

			Thread.sleep(Configurations.MAVEN_REPOSITORY_ACCESS_INTERVAL * 1000);
		} catch (Exception e) {
			LOG.error("Error accessing MVNRepository: " + e.getMessage());
		}

		if (listener != null) {
			listener.onUncategorizedComponent(component);
		}

		return this;
	}

	/**
	 * Navigate to MVNRepository and parse its response
	 * <p>
	 * Concatenate {@link MavenMiner#MAVEN_REPO_URL} with each components' import
	 * and name to produce a valid MVNRepository's URL.
	 * 
	 * @return
	 * @throws IOException
	 */
	private String getRawHTML() throws IOException {
		String address = Configurations.MAVEN_REPO_URL + component.getImport() + "/" + component.getName();
		LOG.info("Contacting MVNRepository to mine metadata for: " + component.getKey());

		URL url = new URL(address);

		URLConnection conn = url.openConnection();
		InputStream is = conn.getInputStream();

		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		String html = br.lines().collect(Collectors.joining());

		return html;
	}

	/**
	 * After contacting the web page, its content is parsed to obtain components'
	 * metadata.
	 * <p>
	 * If the output of the web page changes, this routine must be reviewed.
	 *
	 * @param html raw html response from MVNRepository
	 * @return instance of MavenMiner (this)
	 */
	private MavenMiner fillCategory(String html) {
		int index = html.indexOf(METADATA_MARKUPS);
		if (index >= 0) {
			html = html.substring(index);

			index = html.indexOf(CATEGORY_MARKUPS[0]);
			if (index >= 0) {
				html = html.substring(index);

				index = html.indexOf(CATEGORY_MARKUPS[1]);
				if (index >= 0) {
					component.setCategory(
							html.substring(index + CATEGORY_MARKUPS[1].length(), html.indexOf(CATEGORY_MARKUPS[2])));
				}
			}
		}

		return this;
	}

	/**
	 * Some components' metadata will also enclose some tags with extra information
	 * about how the components have been categorized.
	 * <p>
	 * Tags can help the categorization of components that haven't been categorized
	 * by MVNRepository.
	 * 
	 * @param html raw html response from MVNRepository
	 * @return instance of MavenMiner (this)
	 */
	private MavenMiner fillTags(String html) {
		component.setTags(new HashSet<>());

		int index = html.indexOf(METADATA_MARKUPS);
		if (index >= 0) {
			html = html.substring(index);

			index = html.indexOf(TAGS_MARKUPS[0]);
			if (index >= 0) {
				html = html.substring(index);

				while ((index = html.indexOf(TAGS_MARKUPS[1])) >= 0) {
					String tag = html.substring(index + TAGS_MARKUPS[1].length() + 1,
							(index = html.indexOf(TAGS_MARKUPS[2])));
					component.getTags().add(tag);

					html = html.substring(index + TAGS_MARKUPS[2].length());
				}
			}
		}

		return this;
	}

}
