package org.archknowledge.mining;

import org.archknowledge.JavaExecutor;
import org.archknowledge.model.ThirdPartyComponent;

/**
 * Listener to keep track of uncategorized components
 * <p>
 * {@link MavenMiner} may fail to retrieve information about the categorization
 * of components/concerns, i.e., the metadata obtained for some components may
 * not include information about their categories. An instance of this listener
 * can be injected into {@link MavenMiner} to keep track of all uncategorized
 * components. Later on, {@link JavaExecutor} saves the list of components as a
 * json document to allow the manual insertion of the missing categories.
 * 
 * @author Luis Paulo
 */
public interface UncategorizedComponentListener {

	/**
	 * {@link MavenMiner} calls this method whenever it fails to determine a
	 * category from a component's metadata
	 * 
	 * @param component
	 * @return instance of this (UncategorizedComponentListener)
	 */
	public UncategorizedComponentListener onUncategorizedComponent(ThirdPartyComponent component);

}
