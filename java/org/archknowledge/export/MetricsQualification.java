package org.archknowledge.export;

import java.util.ArrayList;
import java.util.List;

/**
 * Routines to obtain qualified values from numeric metrics
 * <p>
 * Each method tries to determine a qualified value for a metric. Ideally, all
 * qualifications must comply with the values contained in
 * {@link Qualification}.
 * 
 * @author Luis Paulo
 */
public class MetricsQualification {

	public enum Qualification {
		HIGH, MODERATE, SLIGHT, UNKOWN_OR_ERROR;
	}

	public static Qualification toQualifiedICD(double ICD) {
		Qualification QICD = Qualification.UNKOWN_OR_ERROR;

		if (ICD < 0.30) {
			QICD = Qualification.SLIGHT;
		} else if (ICD >= 0.30 && ICD < 0.60) {
			QICD = Qualification.MODERATE;
		} else if (ICD >= 0.60 && ICD <= 1) {
			QICD = Qualification.HIGH;
		}

		return QICD;
	}

	public static Qualification toQualifiedDedication(double ICD, double MD) {
		Qualification QD = Qualification.UNKOWN_OR_ERROR;
		Qualification QICD = toQualifiedICD(ICD);

		if ((QICD == Qualification.MODERATE) || (QICD == Qualification.HIGH)) {
			if (MD < 0.30) {
				QD = Qualification.SLIGHT;
			} else if (MD >= 0.30 && MD < 0.60) {
				QD = Qualification.MODERATE;
			} else if (MD >= 0.60 && MD <= 1) {
				QD = Qualification.HIGH;
			}
		} else {
			QD = Qualification.SLIGHT;
		}

		return QD;
	}

	public static Qualification toQualifiedDedication(double MD) {
		Qualification QMD = Qualification.UNKOWN_OR_ERROR;

		if (MD < 0.30) {
			QMD = Qualification.SLIGHT;
		} else if (MD >= 0.30 && MD < 0.60) {
			QMD = Qualification.MODERATE;
		} else if (MD >= 0.60 && MD <= 1) {
			QMD = Qualification.HIGH;
		}

		return QMD;
	}

	public static List<Qualification> toQualifications(List<String> strings) {
		List<Qualification> qualifications = new ArrayList<>();

		for (String string : strings) {
			qualifications.add(Qualification.valueOf(string));
		}

		return qualifications;
	}

}
