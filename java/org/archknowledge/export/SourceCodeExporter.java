package org.archknowledge.export;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * A simple source code exporter
 * 
 * @author luisp
 */
public class SourceCodeExporter extends SimpleFileVisitor<Path> {

	private String sourceCode;
	private String filePath;

	private Path sourcePath;
	private ZipOutputStream zipOutputStream;

	public SourceCodeExporter configure(String filePath) {
		this.filePath = filePath;

		return this;
	}
	
	public SourceCodeExporter configure(String sourceCode, String filePath) {
		this.sourceCode = sourceCode;
		this.filePath = filePath;

		return this;
	}

	public String toHEX(byte[] bytes) {
		StringBuilder hex = new StringBuilder();

		for (byte b : bytes) {
			hex.append(String.format("%02x", b));
		}

		return hex.toString();
	}

	private String newHash() throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String data = (new Date()).getTime() + "---XXX---" + sourceCode;

		MessageDigest digest = MessageDigest.getInstance("SHA-256");
		byte[] hash = digest.digest(data.getBytes("UTF-8"));

		return toHEX(hash);
	}

	public String export() throws IOException, NoSuchAlgorithmException {
		return export(sourceCode, filePath + "sourcecode/" + newHash() + ".java");
	}

	public String export(String sourceCode, String filePath) throws IOException, NoSuchAlgorithmException {
		if (!sourceCode.equals("")) {
			File file = new File(filePath);

			BufferedWriter bw = new BufferedWriter(new FileWriter(file));
			bw.write(sourceCode);
			bw.close();
		}

		return filePath;
	}

	@Override
	public FileVisitResult visitFile(Path file, BasicFileAttributes attributes) throws IOException {
		Path targetFile = sourcePath.relativize(file);

		zipOutputStream.putNextEntry(new ZipEntry(targetFile.toString()));

		byte[] bytes = Files.readAllBytes(file);
		zipOutputStream.write(bytes, 0, bytes.length);
		zipOutputStream.closeEntry();

		return FileVisitResult.CONTINUE;
	}

	public SourceCodeExporter deleteSourceFiles() {
		File directory = new File(filePath + "sourcecode");
		
		if (!directory.exists()) {
			directory.mkdirs();
		}

		for (File file : directory.listFiles()) {
			if (!file.isDirectory()) {
				file.delete();
			}
		}

		return this;
	}

	public SourceCodeExporter compress(boolean deleteAfterCompression) throws IOException {
		sourcePath = Paths.get(filePath + "sourcecode");
		
		File zipFile = new File(filePath + "sourcecode.zip");
		if (zipFile.exists()) {
			zipFile.delete();
		}

		zipOutputStream = new ZipOutputStream(new FileOutputStream(zipFile));
		Files.walkFileTree(sourcePath, this);
		zipOutputStream.close();

		if (deleteAfterCompression) {
			deleteSourceFiles();
		}

		return this;
	}
}
