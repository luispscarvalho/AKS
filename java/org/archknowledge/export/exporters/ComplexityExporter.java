package org.archknowledge.export.exporters;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.archknowledge.export.Exporter;
import org.archknowledge.export.SourceCodeExporter;
import org.archknowledge.mining.miners.ComplexityMiner;
import org.archknowledge.persistence.AksDAO;
import org.archknowledge.persistence.StaticDAO;
import org.bson.Document;

/**
 * Export the complexity of classes associated with concerns
 * <p>
 * This class iterates through complexity data mined by {@link ComplexityMiner}
 * (must be executed first) and exports the accumulated WMC's and AMW's values
 * of source artifacts.
 * 
 * @author luisp
 */
public class ComplexityExporter extends Exporter {

	@Override
	public String configureHeader() {
		return "project,dimension,label,reference,date,concern,file,sourcecode,imports,wmc,amw";
	}

	/**
	 * Export the complexity associated with concerns
	 * 
	 * @return instance of exporter (this)
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Exporter export() throws Exception {
		prepare();

		AksDAO dao = AksDAO.newComplexityDAO();
		List<Document> complexities = dao.findMany(null, null);

		Map<Date, List<String>> orderedLines = new TreeMap<>();
		// for each change...
		for (Document complexity : complexities) {
			String reference = complexity.getString("reference");

			Document frame = StaticDAO.getFrame(reference);
			Date date = frame.getDate("date");

			// ...let's keep all lines in map ordered by reference's date...
			List<String> lines = new ArrayList<>();
			if (orderedLines.containsKey(date)) {
				lines = orderedLines.get(date);
			} else {
				orderedLines.put(date, lines);
			}

			Set<String> imports = new HashSet<>();
			imports.addAll((List<String>) complexity.get("imports"));

			// ... save the source code...
			String sourceCode = new SourceCodeExporter().configure(complexity.getString("source"), this.getFilePath()).export();

			// ... and add a line for every measured complexity
			String line = getProject() + "," + complexity.getString("dimension") + "," + frame.getString("label") + ","
					+ reference + "," + date + "," + complexity.getString("concern") + ","
					+ complexity.getString("filename") + "," + sourceCode + "," + allStringsInOneString(imports) + ","
					+ getComplexities(complexity);
			lines.add(line);
		}

		for (List<String> lines : orderedLines.values()) {
			getLines().addAll(lines);
		}

		return super.exportLines();
	}

	@SuppressWarnings("unchecked")
	private String getComplexities(Document concern) {
		String line = "0,0";

		int ncomplexities = 0;
		double wmc = 0.0, amw = 0.0;

		List<Document> complexities = (List<Document>) concern.get("complexities");
		for (Document complexity : complexities) {
			if (canCalculateComplexity(complexity)) {
				wmc += complexity.getInteger("WMC", 0) + 0.0;
				amw += complexity.getDouble("AMW");

				ncomplexities += 1;
			}
		}

		if (ncomplexities > 0) {
			line = (int) Math.floor(wmc / ncomplexities) + "," + (int) Math.floor(amw / ncomplexities);
		}

		return line;
	}

	/**
	 * Determine if the complexity can be calculated
	 * <p>
	 * Extensions of this class may decide on the special occasions in which the
	 * complexity can be calculated. For instance, {@link ComplexityBySmellExporter}
	 * allows the calculation only if a concern is associated with a class affected
	 * by smells
	 * <p>
	 * Default is YES, can calculate the complexity
	 * 
	 * @param complexity a document containing information about the complexity
	 * @return true if the calculation is allowed
	 */
	public boolean canCalculateComplexity(Document complexity) {
		return true;
	}

}
