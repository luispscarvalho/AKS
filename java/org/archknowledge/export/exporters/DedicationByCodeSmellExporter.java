package org.archknowledge.export.exporters;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bson.Document;
import org.bson.types.ObjectId;
import org.repositoryminer.metrics.codesmell.CodeSmellId;
import org.repositoryminer.metrics.persistence.CodeAnalysisDAO;

import com.mongodb.client.model.Projections;

/**
 * Export the dedication of classes that host code smells
 * <p>
 * Considering that a source code artifact can contain more than one class
 * (inner classes), this exporter exports the distinct types of smell found in
 * each class. This means: if an artifact encapsulates two classes that host god
 * classes (one god class per class) it will add a list class + smell pairs to
 * the dataset.
 * <p>
 * More info can be found in the super class, {@link DedicationExporter}
 * 
 * @author Luis Paulo
 */
public class DedicationByCodeSmellExporter extends DedicationExporter {

	@Override
	public String configureHeader() {
		return super.configureHeader() + ",godclass,brainclass,dataclass";
	}

	@Override
	@SuppressWarnings("unchecked")
	public String addMoreInformation(String line, double ICo, Document analysis) {
		// we need a rm_code_analysis document get the list of smells
		analysis = getCodeAnalysis(analysis.getObjectId("id"));

		Map<CodeSmellId, Set<String>> smells = new HashMap<>();
		smells.put(CodeSmellId.GOD_CLASS, new HashSet<>());
		smells.put(CodeSmellId.DATA_CLASS, new HashSet<>());
		smells.put(CodeSmellId.BRAIN_CLASS, new HashSet<>());

		for (Document clazz : (List<Document>) analysis.get("classes")) {
			String name = clazz.getString("name");
			name = name.substring(name.lastIndexOf(".") + 1);

			List<String> list = (List<String>) clazz.get("codesmells");
			for (String smell : list) {
				if (CodeSmellId.valueOf(smell).equals(CodeSmellId.GOD_CLASS)) {
					smells.get(CodeSmellId.GOD_CLASS).add(name);
				} else if (CodeSmellId.valueOf(smell).equals(CodeSmellId.DATA_CLASS)) {
					smells.get(CodeSmellId.DATA_CLASS).add(name);
				} else if (CodeSmellId.valueOf(smell).equals(CodeSmellId.BRAIN_CLASS)) {
					smells.get(CodeSmellId.BRAIN_CLASS).add(name);
				}
			}
		}

		return line + "," + allStringsInOneString(smells.get(CodeSmellId.GOD_CLASS), "NONE") + ","
				+ allStringsInOneString(smells.get(CodeSmellId.DATA_CLASS), "NONE") + ","
				+ allStringsInOneString(smells.get(CodeSmellId.BRAIN_CLASS), "NONE");
	}

	private Document getCodeAnalysis(ObjectId id) {
		CodeAnalysisDAO dao = new CodeAnalysisDAO();

		return dao.findById(id, Projections.include("classes"));
	}

}
