package org.archknowledge.export.exporters;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.archknowledge.export.Exporter;
import org.archknowledge.export.MetricsQualification;
import org.archknowledge.export.SourceCodeExporter;
import org.archknowledge.mining.miners.DedicationMiner;
import org.archknowledge.persistence.AksDAO;
import org.archknowledge.persistence.StaticDAO;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.repositoryminer.metrics.persistence.CodeAnalysisDAO;

import com.mongodb.client.model.Projections;

/**
 * Export/Create the dedication dataset
 * <p>
 * Two main metrics are obtained from the execution of this exporter: (i)
 * Methods Dedication (MD) which is calculated as the number of methods linked
 * to the implementation of a concern; and (ii) Dedication to Concern
 * (DtC) as a qualified metric that represents the strength of the association
 * between a concern and a source code artifact.
 * <p>
 * The export routines require the previous execution of {@link DedicationMiner}
 * 
 * @author Luis Paulo
 */
public class DedicationExporter extends Exporter {

	protected int NOR, NOM;
	protected double MD;
	private boolean useICD = true;
	private boolean processEmptyMethods = true;

	@Override
	public String configureHeader() {
		return "project,dimension,date,label,reference,concern,analysis,imports,file,sourcecode,loc,noc,icd,qicd,nom,nor,md,dtc";
	}

	public void configure(boolean processEmptyMethods, boolean useICD) {
		this.useICD = useICD;
		this.processEmptyMethods = processEmptyMethods;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Exporter export() throws Exception {
		prepare();

		AksDAO dao = AksDAO.newDedicationDAO();
		List<Document> dedications = dao.findMany(null, null);

		Map<Date, List<String>> orderedLines = new TreeMap<>();
		// for each change...
		for (Document dedication : dedications) {
			String reference = dedication.getString("reference");
			
			Document frame = StaticDAO.getFrame(reference);
			Date date = frame.getDate("date");

			// ...let's order all complexity by reference's date...
			List<String> lines = new ArrayList<>();
			if (orderedLines.containsKey(date)) {
				lines = orderedLines.get(date);
			} else {
				orderedLines.put(date, lines);
			}

			Document analysis = (Document) dedication.get("code_analysis");
			double ICD = analysis.getDouble("ICD");

			String sourceCode = new SourceCodeExporter().configure(analysis.getString("source"), this.getFilePath())
					.export();

			MetricsQualification.Qualification DTC = calculateDedication(ICD,
					(List<Document>) analysis.get("classes"));
			if (DTC != MetricsQualification.Qualification.UNKOWN_OR_ERROR) {
				Set<String> imports = new HashSet<>();
				imports.addAll((List<String>) analysis.get("imports"));
				// ... and save all successfully qualified dedications
				String line = getProject() + "," + dedication.getString("dimension") + "," + date + ","
						+ frame.getString("label") + "," + reference + "," + dedication.getString("concern") + ","
						+ analysis.getObjectId("id").toString() + "," + allStringsInOneString(imports) + ","
						+ analysis.getString("filename") + "," + sourceCode + "," + getLOC(analysis.getObjectId("id"))
						+ "," + analysis.getInteger("NOC", 0) + "," + ICD + ","
						+ MetricsQualification.toQualifiedICD(ICD) + "," + NOM + "," + NOR + "," + MD + ","
						+ DTC;
				line = addMoreInformation(line, ICD, analysis);

				lines.add(line);
			}
		}

		for (List<String> lines : orderedLines.values()) {
			getLines().addAll(lines);
		}

		return super.exportLines();
	}

	/**
	 * Fill extra information at the end of each exported line
	 * <p>
	 * For instance, {@link DedicationByCodeSmellExporter} adds a list of code
	 * smells associated with the analysis
	 * 
	 * @param line
	 * @param ICD
	 * @param analysis
	 * @return extended line with extra information
	 */
	public String addMoreInformation(String line, double ICD, Document analysis) {
		return line;
	}

	/**
	 * Retrieve the Lines Of Code (LOC) from an analysis
	 * 
	 * @param analysisId
	 * @return the value of LOC
	 */
	@SuppressWarnings("unchecked")
	public int getLOC(ObjectId analysisId) {
		CodeAnalysisDAO dao = new CodeAnalysisDAO();
		Document analysis = dao.findById(analysisId, Projections.include("metrics"));
		Integer LOC = getMetricValue("LOC", (List<Document>) analysis.get("metrics"));

		return LOC;
	}

	@SuppressWarnings("unchecked")
	protected MetricsQualification.Qualification calculateDedication(double ICD, List<Document> classes) {
		NOR = NOM = 0;
		for (Document clazz : classes) {
			List<Document> methods = (List<Document>) clazz.get("methods");
			for (Document method : methods) {
				if (canProcess(method)) {
					if (isDedicated(method)) {
						NOR++;
					}

					NOM++;
				}
			}
		}

		return toQualification(ICD);
	}

	private boolean canProcess(Document method) {
		boolean can = true;

		if (!processEmptyMethods && method.getBoolean("is_empty")) {
			can = false;
		}

		return can;
	}

	private MetricsQualification.Qualification toQualification(double ICD) {
		MetricsQualification.Qualification qualification = MetricsQualification.Qualification.UNKOWN_OR_ERROR;

		MD = 0.0;
		if (NOM > 0) {
			MD = NOR * 1.0 / NOM;
			MD = (Math.floor(MD * 100)) / 100;

			if (useICD) {
				qualification = MetricsQualification.toQualifiedDedication(ICD, MD);
			} else {
				qualification = MetricsQualification.toQualifiedDedication(MD);
			}
		}

		return qualification;
	}

	protected boolean isDedicated(Document method) {
		boolean is = false;

		if (method.getInteger("NOR") > 0) {
			is = true;
		}

		return is;
	}

}
