package org.archknowledge.export.exporters;

import java.util.List;

import org.bson.Document;
import org.repositoryminer.metrics.codesmell.CodeSmellId;

/**
 * Export the complexity of concerns associated with artifacts that host code
 * smells.
 * <p>
 * This class extends {@link ComplexityExporter} in order to filter data. The
 * only information being allowed is the one in which concerns are associated
 * with artifacts that host code smells. 
 * 
 * @author Luis Paulo
 */
public class ComplexityBySmellExporter extends ComplexityExporter {

	private List<CodeSmellId> smells;

	public ComplexityBySmellExporter configure(List<CodeSmellId> smells) {
		return setCodeSmells(smells);
	}

	private ComplexityBySmellExporter setCodeSmells(List<CodeSmellId> smells) {
		this.smells = smells;

		return this;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean canCalculateComplexity(Document complexity) {
		boolean can = false;

		List<String> zmells = (List<String>) complexity.get("codesmells");

		for (String zmell : zmells) {
			for (CodeSmellId smell : smells) {
				if (CodeSmellId.valueOf(zmell).equals(smell)) {
					can = true;

					break;
				}
			}
		}

		return can;
	}

}
