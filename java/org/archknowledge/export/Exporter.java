package org.archknowledge.export;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.archknowledge.persistence.DAO;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * CSV-based exporter
 * <p>
 * Save a dataset as a CSV file. Each exporter in the exporters package must
 * define the specific strategies to format the CSV's header and rows. The
 * exporting routines must be written by overloading the {@link #export()}
 * method. Header can be defined by rewriting the {@link #getHeader()} method.
 * <p>
 * The {@link Exporter#configure(String, String, String)} and
 * {@link Exporter#setLines(List)} methods must be called first, prior to saving
 * data to the dataset.
 * 
 * @author Luis Paulo
 */
public abstract class Exporter {

	protected static final Logger LOG = LoggerFactory.getLogger(Exporter.class);

	private String project;
	private String fileName;
	private String filePath;
	private String header;
	private String key;

	private List<String> lines;
	private boolean addLineNumber = true;

	/**
	 * Configure the exporter
	 * <p>
	 * It must be called prior to exporting the data in order to information about
	 * the new CSV file:
	 * 
	 * @param project  name of the project being exported
	 * @param filePath the destiny filepath of the CSV dataset
	 * @param fileName the destiny filename of the CSV dataset
	 * @param key      a generic identification of the exporter
	 * @return instance of this (Exporter)
	 */
	public Exporter configure(String project, String filePath, String fileName, String key) {
		setProject(project);
		setFilePath(filePath);
		setFileName(fileName);
		setKey(key);

		setHeader(configureHeader());
		setLines(new ArrayList<String>());

		return this;
	}

	public void prepare() {
		DAO.configure(getKey());

		clearLines();
	}

	public Exporter setProject(String project) {
		this.project = project;

		return this;
	}

	public String getProject() {
		return project;
	}

	public Exporter setFileName(String fileName) {
		this.fileName = fileName;

		return this;
	}

	public String getFileName() {
		return fileName;
	}

	public Exporter setFilePath(String filePath) {
		this.filePath = filePath;

		return this;
	}

	public String getFilePath() {
		return filePath;
	}

	public Exporter setHeader(String header) {
		this.header = header;

		return this;
	}

	public String getHeader() {
		return header;
	}

	public Exporter setLines(List<String> lines) {
		this.lines = lines;

		return this;
	}

	public Exporter setKey(String key) {
		this.key = key;

		return this;
	}

	public String getKey() {
		return key;
	}

	public List<String> getLines() {
		return lines;
	}

	public void clearLines() {
		lines.clear();
	}

	public Exporter linesNumberOn() {
		this.addLineNumber = true;

		return this;
	}

	public Exporter linesNumberOff() {
		this.addLineNumber = false;

		return this;
	}

	/**
	 * Export all lines to a CSV file
	 * <p>
	 * Prior to calling export, a collection of lines must be supplied using the
	 * {@link Exporter#setLines(List)} method.
	 * 
	 * @return instance of Exporter (this)
	 * @throws Exception
	 * @throws IOException
	 */
	protected Exporter exportLines() throws Exception {
		File file = new File(filePath + fileName + ".csv");
		if (!file.delete()) {
			LOG.info("Unable do delete: " + file.getPath());
		}

		PrintWriter writer = new PrintWriter(file);
		if (header != null) {
			writer.write("num," + header + "\n");
		}

		int num = 0;
		for (String line : lines) {
			if (addLineNumber) {
				writer.write((++num) + "," + line + "\n");
			} else {
				writer.write(line + "\n");
			}
		}

		writer.flush();
		writer.close();

		LOG.info("File '" + file.getName() + "' created");

		return this;
	}
	
	public String allStringsInOneString(List<Document> documents, String stringField) {
		Set<String> items = new HashSet<>();

		for (Document document : documents) {
			items.add(document.getString(stringField));
		}

		return allStringsInOneString(items);
	}

	public String allStringsInOneString(Set<String> items, String stringIfEmpty) {
		String string = allStringsInOneString(items);

		if (string.isEmpty()) {
			string = stringIfEmpty;
		}

		return string;
	}

	public String allStringsInOneString(Set<String> items) {
		String string = "";

		for (String item : items) {
			string += (string.isEmpty() ? item : "|" + item);
		}

		return string;
	}

	public String allStringsInOneString(Map<String, String> items) {
		String string = "";

		for (Map.Entry<String, String> entry : items.entrySet()) {
			String s = entry.getKey() + ":" + entry.getValue();
			string += (string.isEmpty() ? s : "|" + s);
		}

		return string;
	}

	public Exporter export(List<String> lines) throws Exception {
		this.setLines(lines);

		return exportLines();
	}

	/**
	 * Retrieve the value of a metric from a list of metrics + values
	 * 
	 * @param metricName the name of the desired metric to return value from
	 * @param metrics    list of metrics
	 * @return the value of the metric
	 */
	@SuppressWarnings("unchecked")
	public <T> T getMetricValue(String metricName, List<Document> metrics) {
		T value = null;

		for (Document metric : metrics) {
			if (metricName.equals(metric.getString("name"))) {
				value = (T) metric.get("value");

				break;
			}
		}

		return value;
	}

	/**
	 * Configure the header of the csv file
	 * 
	 * @return a list of columns names separated by commas
	 */
	public abstract String configureHeader();

	/**
	 * Execute special export routines
	 * <p>
	 * Usually, exporters (re)implement this method in order to execute special
	 * strategies to export data. E.g., mining a dataset from the database.
	 * 
	 * @return instance of Exporter (this)
	 * @throws Exception
	 */
	public abstract Exporter export() throws Exception;

}
