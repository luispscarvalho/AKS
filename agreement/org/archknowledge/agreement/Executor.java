package org.archknowledge.agreement;

import java.io.BufferedReader;
import java.io.FileReader;

import org.archknowledge.export.SourceCodeExporter;
import org.archknowledge.persistence.DAO;
import org.bson.Document;
import org.bson.types.ObjectId;

import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Projections;

public class Executor {

	private static final String DATASET_PATH = "/aks/datasets/domains";
	private static final String AGREEMENT_PATH = DATASET_PATH + "/agreement/";

	private static final String SPLIT_BY = ",";

	private static final String DATABASE_URI = "mongodb://172.28.1.11";
	private static String key = "";
	private static DAO dao = null;

	private static String getSourceCode(String project, String concern, String analysis) {
		String source = "";

		if (!key.equals(project)) {
			key = project;

			DAO.configure(DATABASE_URI, key);
			dao = new DAO("aks_dedication");
		}

		Document dedication = dao.findOne(
				Filters.and(Filters.eq("concern", concern), Filters.eq("code_analysis.id", new ObjectId(analysis))),
				Projections.include("code_analysis.source"));
		if (!dedication.isEmpty()) {
			Document codeAnalysis = (Document) dedication.get("code_analysis");
			source = codeAnalysis.getString("source");
		}

		return source;
	}

	private static void getSourceCode(String path) throws Exception {
		BufferedReader br = new BufferedReader(new FileReader(path + "agreement.csv"));

		SourceCodeExporter codeExporter = new SourceCodeExporter().configure(path).deleteSourceFiles();

		// avoid reading file's header
		String line = br.readLine();
		while ((line = br.readLine()) != null) {
			// comma as separator
			String[] columns = line.split(SPLIT_BY);

			String project = columns[1].replaceAll("\"", "");
			String analysis = columns[2].replaceAll("\"", "");
			String concern = columns[3].replaceAll("\"", "");
			String fileName = columns[6].replaceAll("\"", "");

			String source = getSourceCode(project, concern, analysis);
			codeExporter.export(source, path + "sourcecode/" + fileName);
		}

		br.close();

		codeExporter.compress(true);
	}

	public static void main(String[] args) throws Exception {
		getSourceCode(AGREEMENT_PATH);
	}

}
