AKS is a mining tool capable of extracting information about concerns 
from systems' POM and Gradle files. It uses the information about injected 
third-party components to locate concerns in the source code.

It is necessary to clone software projects prior to executing AKS. The projects 
can be found in GIT-enabled platforms such as GITHUB and GITLAB.

Download all content of AKS' release folder: 

https://gitlab.com/luispscarvalho/AKS/tree/master/release. 

The folder contains AKS' executable JAR and examples of configuration files. 

AKS is dependent on MongoDb. You must install MongoDb and configure AKS to 
access it via the "mining.config" file. 

We have shipped some R analysis scripts with AKS. So, installing 
R language's core libraries and files (https://www.r-project.org/) is required. 
We recommend R Studio to manipulate our scripts (in the ./analysis directory).

For further information, consult our tutorials in the ./docs directory. The following
are video-tutorials (in portuguese) that demonstrate how to use AKS:

	https://www.dropbox.com/s/a1hfx9ow0cubdha/aks1.mp4?dl=0
	
	https://www.dropbox.com/s/zj0p0oi9i67nr2o/aks2.mp4?dl=0
	
	https://www.dropbox.com/s/xns5plhxk5gkanx/aks3.mp4?dl=0
	
	https://www.dropbox.com/s/1jk0kd96r7k2gqj/aks4.mp4?dl=0

Feel free to contact me: luispscarvalho@gmail.com